#! /bin/bash

# Quit on errors
set -e

# Load env vars
set -o allexport; source .env; set +o allexport
export ORIG_PWD=`pwd`

sudo hostname ${DESIRED_HOSTNAME}

# Start firmware DB
cd ./honeypot/high_fidelity/FirmAE/
./init.sh

# pre-build firmware. This takes a while so it's more efficient to do it outside a docker container. Only has to be 1ce anyway.
for device in `ls ../devices/ | grep -v 'Dockerfile'`
do
    # It's possible for emulation to silently fail. This can be noticed by means of no tap devices ever being created. To confirm this issue, check `scratch/${IID}/makeNetwork.log` for QEMU saying that it failed to create a `.ram` file. The reason is insufficient RAM on the host.
    # Add a swap file: https://aws.amazon.com/premiumsupport/knowledge-center/ec2-memory-swap-file/
    set +e
    # we check for the return code of `grep` so can't just `|| true` to prevent exiting on error
    sudo ./run.sh -c `cat ../devices/"${device}"/brand` ../devices/"${device}"/firmware.zip 2>&1 | grep 'Network reachable'
    NEW_EMULATION_DONE=$?
    set -e

    # rewrite main IP of emulation to what we want it to be
    IID=`./scripts/util.py get_iid ../devices/"${device}"/firmware.zip 127.0.0.1`
    EMULATION_RESULT_DIR="$(pwd)/scratch/${IID}/"

    if [[ $NEW_EMULATION_DONE -eq 0 ]]
    then
        sudo rm ${EMULATION_RESULT_DIR}ip_rewritten >/dev/null || true
    fi

    if [[ ! -f ${EMULATION_RESULT_DIR}ip_rewritten ]]
    then
        FROM_IP=`cat ../devices/"${device}"/from_ip`
        TO_IP=`cat ../devices/"${device}"/to_ip`
        FROM_GATEWAY=`cat ../devices/"${device}"/from_gateway`

        cd ${ORIG_PWD}/other/binary_rewrite/
        ./rewrite.sh ${EMULATION_RESULT_DIR} ${FROM_IP} ${TO_IP} ${FROM_GATEWAY} ${IOT_SUBNET_GATEWAY}
        cd -
        sudo chown root:root ${EMULATION_RESULT_DIR}image.raw
        sudo touch ${EMULATION_RESULT_DIR}ip_rewritten
    fi
done
cd ${ORIG_PWD}

cd ./honeypot/host_services/firewall/
# This is not in `setup.sh` as `iptables` runs in memory, so a reboot requires this to be rerun.
echo '* Creating firewall rules'
./start_fw_rules.sh
echo '* Starting auto-blocking of scanners'
./start_scanner_blocking.sh
cd ${ORIG_PWD}

cd ./honeypot/host_services/network_tap/
echo '* Starting tcpdump'
./start_capture.sh
cd ${ORIG_PWD}

cd ./honeypot/host_services/healthcheck/
echo '* Starting IoT emulation (QEMU) healthcheck'
./start_healthcheck.sh
cd ${ORIG_PWD}

cd ./honeypot/host_services/abuse_check/
echo '* Starting abuse checker'
./start_abuse_check.sh
cd ${ORIG_PWD}

cd ./honeypot/host_services/telnet_proxy/
echo '* Starting telnet proxy'
./start_telnet_proxy.sh
cd ${ORIG_PWD}

cd ./honeypot/host_services/update_creds/
echo '* Starting telnet proxy creds updater'
./start_update_creds.sh
cd ${ORIG_PWD}

cd ./honeypot/host_services/log_handling/
echo '* Starting log handling'
./start_log_handling.sh
cd ${ORIG_PWD}

cd ./honeypot/host_services/honeytoken_bot/
echo '* Starting honeytoken distribution bot'
./start_honeytoken_bot.sh
cd ${ORIG_PWD}

# Start emulation
# gotta do this weird dance as, without it, FirmAE fails to connect to the DB on the host.
sudo docker-compose up -d
sudo docker-compose stop
cd ./honeypot/high_fidelity/FirmAE/
./init.sh
cd ${ORIG_PWD}
sudo docker-compose up
