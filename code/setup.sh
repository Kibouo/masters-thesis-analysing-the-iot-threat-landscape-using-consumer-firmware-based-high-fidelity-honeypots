#! /bin/bash

# Quit on errors
set -e

export ORIG_PWD=$(pwd)

echo -n "Host name to show in logs: "
read DESIRED_HOSTNAME
sudo hostname ${DESIRED_HOSTNAME}

# Load env variables
if [ ! -f .env ]; then
    echo ".env file not found. Generating..."
    cd ./other/gen_env/
    ./gen_env.sh
    cd ${ORIG_PWD}
fi
set -o allexport; source .env; set +o allexport

# Ensure system is up to date
sudo apt-get --fix-missing update # fix missing dependencies
sudo apt-get --yes --fix-broken install
sudo apt-get --yes upgrade
# for: iptables IP blacklist
sudo apt-get --yes install ipset
# for: binary rewrite
sudo apt-get --yes install flex
# for: docker devicemapper
sudo apt-get --yes install thin-provisioning-tools docker docker.io
# for: cowrie
sudo add-apt-repository ppa:deadsnakes/ppa # python3.7
sudo apt-get --yes install libffi-dev libssl-dev python3.7 virtualenv

# Install latest docker-compose (`apt` is behind several versions)
sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

# Setup xfs & devicemapper for Docker. Needed as sending context includes "uploading" FirmAE to docker, which is too big in size for normal docker contexts.
# see https://docs.docker.com/storage/storagedriver/device-mapper-driver/
sudo mkdir -p /etc/docker/
# NOTE: dev only (rebuilding docker containers)
#sudo cp ./other/dockerd/daemon.json /etc/docker/daemon.json
sudo systemctl enable docker
sudo systemctl start docker

# disable ipv6 as it causes `github-releases.githubusercontent.com` to fail resolving
sudo sysctl -w net.ipv6.conf.all.disable_ipv6=1
sudo sysctl -w net.ipv6.conf.default.disable_ipv6=1
sudo sysctl -w net.ipv6.conf.lo.disable_ipv6=1

# Enable remote management but only on the internet facing interface
sudo bash -c 'INTERNET_IF_IP=`cat $(pwd)/.env | grep INTERNET_IF_IP | sed "s/.*=//g"`; echo "ListenAddress ${INTERNET_IF_IP}" >> /etc/ssh/sshd_config'
sudo systemctl restart ssh

# Install FirmAE. Works best on Ubuntu 18.04
git submodule update --init --recursive # prevent surprises
# apply customisations. This is not done through a forked repo as the customisations are highly specific to this project and I wanted to keep everything centralised
cp -r ./honeypot/high_fidelity/FirmAE_custom/* ./honeypot/high_fidelity/FirmAE/

cd ./honeypot/high_fidelity/FirmAE/
./download.sh # download kernels
./install.sh # install deps & setup firmware DB
sudo apt-get --yes clean
sudo ./docker-init.sh # build FirmAE core image
cd ${ORIG_PWD}

cd ./honeypot/host_services/firewall/
./setup_logging.sh
cd ${ORIG_PWD}

cd ./honeypot/host_services/telnet_proxy/
./setup_telnet_proxy.sh
cd ${ORIG_PWD}

cd ./honeypot/host_services/rsyslog/
./setup_rsyslog.sh
cd ${ORIG_PWD}

# Setup log dirs
mkdir -p ${LOG_DIR}mqtt/traffic_gen/
mkdir -p ${LOG_DIR}mqtt/hub/
mkdir -p ${LOG_DIR}tcpdump/

# Start honeypot
cd ./other/honeypot_as_a_service/
./setup_haas.sh
cd ${ORIG_PWD}