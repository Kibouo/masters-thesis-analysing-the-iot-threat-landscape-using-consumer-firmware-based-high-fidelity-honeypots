# Asus RT AC3100
Released in 2018 with numerous patches, including for security fixes, up till 2021 (inclusive).

## Firmware
- release date: 2021/05/18
- name: FW_RT_AC3100_300438643129.zip
- source: https://www.asus.com/Networking-IoT-Servers/WiFi-Routers/ASUS-WiFi-Routers/RT-AC3100/HelpDesk_BIOS/