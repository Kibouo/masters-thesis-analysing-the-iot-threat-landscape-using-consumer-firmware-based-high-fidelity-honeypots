#! /bin/bash

INTERFACE=$(ip a | egrep '^2:' | cut -d' ' -f 2)
INTERNET_IP=$(ip a | egrep '^2:' -A 6 | grep 'inet ' | cut -d ' ' -f 6 | cut -d '/' -f 1)

cat ./default.env \
| sed "s/<internet_if_name>/$(echo ${INTERFACE:0:2}+)/g" \
| sed "s/<user>/`whoami`/g" \
| sed "s/<internet_if_ip>/${INTERNET_IP}/g" \
| tee ${ORIG_PWD}/.env