#! /bin/bash

# Periodically fetches logs from honeypot.

# Quit on errors
set -e

SERVICE_NAME=haas

# Stop service in case we're re-running this setup script
(sudo systemctl stop ${SERVICE_NAME}.service || true) 2>/dev/null
(sudo systemctl disable ${SERVICE_NAME}.service || true) 2>/dev/null

# Generate `.service` file dynamically. This is needed to expand vars.
(cat ./${SERVICE_NAME}.service.template             \
| sed "s~<orig_user>~`whoami`~g"                    \
| sed "s~<start_dir>~${ORIG_PWD}~g"                 \
| sed "s~<docker_compose>~`which docker-compose`~g" \
| sed "s/<hostname>/`hostname`/g"                   \
| sudo tee ${SERVICE_PATH}${SERVICE_NAME}.service) 1>/dev/null

# Start service
sudo systemctl daemon-reload
sudo systemctl start ${SERVICE_NAME}.service
sudo systemctl enable ${SERVICE_NAME}.service