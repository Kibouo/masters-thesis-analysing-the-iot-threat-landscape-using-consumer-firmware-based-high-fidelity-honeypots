#! /bin/bash

# Quit on errors
set -e

# args:
## from env:
# LOG_DIR
# REMOTE_IP
# REMOTE_USER
# PRIV_KEY
# LOCAL_STORAGE

# differentiate remote instances
mkdir -p ${LOCAL_STORAGE}${REMOTE_IP}/

# This could probably be done easier with rsync...
FILES_TO_GET=$(ssh -i ${PRIV_KEY} ${REMOTE_USER}@${REMOTE_IP} 'find '${LOG_DIR}' -name "*.gz"')
for REMOTE_FILE in ${FILES_TO_GET}
do
    REMOTE_CHECKSUM=$(ssh -i ${PRIV_KEY} ${REMOTE_USER}@${REMOTE_IP} "sha1sum ${REMOTE_FILE}" | cut -d ' ' -f 1)
    LOCAL_CHECKSUM='not_found'

    while [[ ${LOCAL_CHECKSUM} != ${REMOTE_CHECKSUM} ]]
    do
        LOCAL_FILE=$(echo ${REMOTE_FILE} | sed "s~${LOG_DIR}~${LOCAL_STORAGE}${REMOTE_IP}/~g")
        mkdir -p ${LOCAL_FILE%/*} # make dir in case it doesn't exist
        scp -i ${PRIV_KEY} ${REMOTE_USER}@${REMOTE_IP}:${REMOTE_FILE} ${LOCAL_FILE}
        LOCAL_CHECKSUM=$(sha1sum ${LOCAL_FILE} | cut -d ' ' -f 1)
    done

    # checksum of downloaded file matches remote. Remote is safe to delete
    ssh -i ${PRIV_KEY} ${REMOTE_USER}@${REMOTE_IP} "rm ${REMOTE_FILE}"
done