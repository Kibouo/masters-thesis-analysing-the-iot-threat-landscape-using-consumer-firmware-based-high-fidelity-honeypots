#! /bin/bash

# Periodically fetches logs from honeypot.

# Quit on errors
set -e

# args:
if [ "$#" -ne 4 ]; then
    echo 'Usage: start_log_fetch.sh <remote_user> <remote_ip> <ssh_priv_key_path> <local_storage>'
fi
REMOTE_USER=${1}
REMOTE_IP=${2}
PRIV_KEY=${3}
LOCAL_STORAGE=${4}

SERVICE_TEMPLATE_NAME=log_fetch
SERVICE_NAME=${SERVICE_TEMPLATE_NAME}_${REMOTE_IP}

# Stop service in case we're re-running this setup script
(sudo systemctl stop ${SERVICE_NAME}.timer || true) 2>/dev/null

# Generate `.service` file dynamically. This is needed to expand env vars.
(cat ./${SERVICE_TEMPLATE_NAME}.service.template             \
| sed "s~<pwd>~`pwd`~g"                             \
| sed "s/<user>/`whoami`/g"                         \
| sed "s~<log_dir>~${LOG_DIR}~g"                    \
| sed "s/<remote_ip>/${REMOTE_IP}/g"                \
| sed "s/<remote_user>/${REMOTE_USER}/g"            \
| sed "s~<priv_key>~${PRIV_KEY}~g"                  \
| sed "s~<local_storage>~${LOCAL_STORAGE}~g"                  \
| sudo tee ${SERVICE_PATH}${SERVICE_NAME}.service) 1>/dev/null
sudo cp ./${SERVICE_TEMPLATE_NAME}.timer ${SERVICE_PATH}${SERVICE_NAME}.timer

# Start service
sudo systemctl daemon-reload
sudo systemctl start ${SERVICE_NAME}.timer