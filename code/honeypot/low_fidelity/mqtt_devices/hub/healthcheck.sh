#! /bin/sh

IP=$(ip route show | grep eth0 | cut -d ' ' -f 8)
timeout 3 mosquitto_sub -t '$$SYS/#' -C 1 -h ${IP} -i healthcheck && echo something_for_grep | grep -v -q Error || exit 1