#! /bin/sh

set -e

(sleep 15 && python3 /hub.py) &
/docker-entrypoint.sh /usr/sbin/mosquitto -c /mosquitto/config/mosquitto.conf