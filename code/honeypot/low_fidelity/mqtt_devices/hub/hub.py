import paho.mqtt.client as mqtt
import time

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Hub functionality online")
    client.subscribe("#")

def on_message(client, userdata, msg):
    if 'update/check' in msg.topic:
        time.sleep(2)
        client.publish('/'.join(msg.topic.split('/')[:-1]) + '/command', payload="{\"run\":\"date '+%Y-%m-%d_%H:%M:%S' > /etc/last_update_check\"}", qos=0, retain=False)

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

client.connect("192.168.0.23", 1883, 60)

client.loop_forever()
