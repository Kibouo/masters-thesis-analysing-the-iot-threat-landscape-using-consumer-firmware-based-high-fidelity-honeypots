#! /bin/bash

set -e

# args:
DEVICE=${1-'*'}
DEVICE_ID=${2-''}

# need bash -c to eval the '*'
TEMPLATE=`bash -c "ls ./${DEVICE}" | grep json | sed 's/\n//g' | shuf -n 1`
echo "[*] using template: ${TEMPLATE}"
cat ${TEMPLATE} | sed "s/<uuid>/`uuidgen`/g" | sed "s/<device_id>/${DEVICE_ID}/g" > ./traffic.json
