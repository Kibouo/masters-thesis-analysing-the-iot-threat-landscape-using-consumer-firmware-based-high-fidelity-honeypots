#! /bin/bash
# requires bash as arithmetic, `(( ))`, is not supported by sh

UUID=$(cat /work/traffic.json | grep clientId | cut -d'"' -f 4)
LAST_LOG_IN_SECS=$(date +%s -r /work/logs/${UUID}.log)
NOW_IN_SECS=$(date +%s)
# client is alive if the last message was < 60 secs ago
(( NOW_IN_SECS - LAST_LOG_IN_SECS < 60 ))