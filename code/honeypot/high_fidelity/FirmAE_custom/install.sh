#!/bin/bash

sudo apt-get update
sudo apt-get install -y curl wget git ruby python3 python3-pip bc
python3 -m pip install coloredlogs

# for docker
sudo apt-get install -y docker.io

# postgresql
sudo apt-get install -y postgresql
sudo /etc/init.d/postgresql restart
sudo -u postgres bash -c "psql -c \"CREATE USER firmadyne WITH PASSWORD 'firmadyne';\""
sudo -u postgres createdb -O firmadyne firmware
sudo -u postgres psql -d firmware < ./database/schema
echo "listen_addresses = '${FW_DB_SUBNET_GATEWAY},127.0.0.1,localhost'" | sudo -u postgres tee --append /etc/postgresql/*/main/postgresql.conf
echo "host all all ${FW_DB_SUBNET_GATEWAY}/24 trust" | sudo -u postgres tee --append /etc/postgresql/*/main/pg_hba.conf

sudo apt install libpq-dev
python3 -m pip install psycopg2 psycopg2-binary

sudo apt-get install -y busybox-static bash-static fakeroot dmsetup kpartx netcat-openbsd nmap python3-psycopg2 snmp uml-utilities util-linux vlan

# for binwalk
curl -Ls https://api.github.com/repos/ReFirmLabs/binwalk/releases/latest | \
  grep -wo "\"https.*tarball.*\"" | sed 's/"//g' | wget -qi - -O binwalk.tar.gz && \
  tar -xf binwalk.tar.gz && \
  cd ReFirmLabs-binwalk-*/ && \
  echo y | ./deps.sh | true
sudo apt-get install -y mtd-utils gzip bzip2 tar arj lhasa p7zip p7zip-full cabextract fusecram cramfsswap squashfs-tools sleuthkit default-jdk cpio lzop lzma srecord zlib1g-dev liblzma-dev liblzo2-dev unzip

git clone https://github.com/devttys0/sasquatch && (cd sasquatch && ./build.sh && cd -)
git clone https://github.com/devttys0/yaffshiv && (cd yaffshiv && sudo python3 setup.py install && cd -)
cd - # back to root of project
sudo cp core/unstuff /usr/local/bin/

python3 -m pip install python-lzo cstruct ubi_reader
sudo apt-get install -y python3-magic openjdk-8-jdk unrar
# NOTE: sometimes binwalk is not found, likely because it's accessed via `sudo run.sh`. Do we need to install it with root? But using `sudo pip` is horrible practice...
python3 -m pip install git+https://github.com/ReFirmLabs/binwalk@772f271 # Release 2.3.1

# for qemu
sudo apt-get install -y qemu-system-arm qemu-system-mips qemu-system-x86 qemu-utils
