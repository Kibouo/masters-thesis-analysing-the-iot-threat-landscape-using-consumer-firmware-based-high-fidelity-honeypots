#! /bin/bash

echo "#! /firmadyne/sh

# This script is called during init of the emulated firmware.
# See \`FirmAE_custom/scripts/makeImage.sh\` and \`makeNetwork.py\` for more details.

# tweak emulated FW to be usable
(sleep 180 && /firmadyne/fix_emulation.sh ${IOT_SUBNET_GATEWAY}) &

# remove setup leftovers
rm /bin/a
rm /.ash_history

# admin telnet, not logged in proxy
# /firmadyne/busybox telnetd -p 2323 -l /firmadyne/sh
" > /work/custom/hook.sh

chmod a+x /work/custom/hook.sh