#! /firmadyne/sh

BUSYBOX=/firmadyne/busybox

# args:
IOT_SUBNET_GATEWAY=${1}

# default gateway setup
${BUSYBOX} ip route delete default || true
${BUSYBOX} ip route add default via ${IOT_SUBNET_GATEWAY}

# MAC setup
# Replace the generic/default 00:ff:ff:ff:ff:ff, which is too obvious.
# MAC address prefixes are vendor identifying. '3C:37:86' is a valid prefix for a Netgear device.
(sleep 60 && ${BUSYBOX} ip link set dev eth0 address '3C:37:86:D7:EF:61') &

# DNS setup
echo 'nameserver 8.8.8.8' > /etc/resolv.conf
echo 'nameserver 8.8.4.4' >> /etc/resolv.conf

# replace FW's busybox with FirmAE's busybox, to give adversaries more tools to play with. Letting them download stuff over the highly limited bandwidth of the emulated network would potentially annoy them and decrease retention rate.
cp ${BUSYBOX} /bin/busybox

# firmadyne's kernel is 4.1.17, but the original FW uses an older one. The `modules` directory contains a dir for this old version.
rm -rf /lib/modules/2.6.36.4brcmarm+/
mkdir -p /lib/modules/4.1.17/kernel/

# there are no TLS certs on the device
# NOTE: despite certs working, the `curl` and `wget` available on the firmware support only up to TLSv1.1 🙃 That's something fun for the attacker to figure out.
mkdir -p /etc/pki/tls/certs/
cp /firmadyne/ca-bundle.crt /etc/pki/tls/certs/
${BUSYBOX} chmod 444 /etc/pki/tls/certs/ca-bundle.crt

# kill unnecessary services
${BUSYBOX} kill -2 `${BUSYBOX} pgrep telnet`

# UPnP setup, reversed from `/usr/sbin/upnpd`
# show "connected" in `WANIPConn1.GetStatusInfo`. Pretends to have a pppoe connection, works together with `libnvram.override/wan_proto`. This is the easiest path starting at `FUN_00023f98:231` into `FUN_00023220`.
echo -n 1 > /tmp/ppp/link.ppp0
# show actual IP address in `WANIPConn1.GetExternalIPAddress`
curl api4.my-ip.io/ip > /firmadyne/libnvram.override/wan_ipaddr