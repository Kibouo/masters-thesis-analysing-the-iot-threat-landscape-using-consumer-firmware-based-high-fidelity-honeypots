# Netgear R7000
Released in 2020 with numerous patches, including for security fixes, up till 2021 (inclusive).

## Firmware
- release date: 2021/01/18
- name: R7000-V1.0.11.116_10.2.100.zip
- source: https://www.netgear.com/support/download/?model=R7000