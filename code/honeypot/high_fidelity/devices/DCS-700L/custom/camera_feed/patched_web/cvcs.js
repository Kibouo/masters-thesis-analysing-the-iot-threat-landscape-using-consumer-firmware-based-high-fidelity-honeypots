var cvcs = {
    FrameRate : 10,
    TimeString : "",
    spkstatus : -2,
    mute : 0,
    audioOut : 1,
    playing : 1,
    scalingFactor : 1,
    Play : function() {
        this.playing = 1;

        if (document.getElementById("feed") == null) return;
        document.getElementById("feed").src = "feed.jpg?cidx=" + Math.random();
    },
    Stop : function() { this.playing = 0; },
    CheckStatus : function() { return this.audioOut; },
    StartAudioOut : function() { this.audioOut = 1; },
    StopAudioOut : function() { this.audioOut  = 0; },
    Mute : function(m) { this.mute             = m; },
    Zoom : function(z) {
        var zoomVideoFeed = document.getElementById("feed");

        if (zoomVideoFeed == null) return;
        zoomVideoFeed.width  = zoomVideoFeed.width / this.scalingFactor;
        zoomVideoFeed.height = zoomVideoFeed.height / this.scalingFactor;
        this.scalingFactor   = z;
        zoomVideoFeed.width  = zoomVideoFeed.width * this.scalingFactor;
        zoomVideoFeed.height = zoomVideoFeed.height * this.scalingFactor;
    },
    GetFrameRate : function() { return this.FrameRate; },
    GetRealTimeData : function() {
        varnow = new Date();
        while (__sec >= 60)
        {
            __sec = __sec - 60;
            __min++;
        }
        while (__min >= 60)
        {
            __min = __min - 60;
            __hour++;
        }
        while (__min < 0)
        {
            __min = __min + 60;
            __hour--;
        }
        while (__hour >= 24)
        {
            __hour = __hour - 24;
            __date++;
        }
        while (__hour < 0)
        {
            __hour = __hour + 24;
            __date--;
        }
        now.setYear(__year);
        now.setMonth(__month - 1);
        now.setDate(__date);
        now.setHours(__hour);
        now.setMinutes(__min);
        now.setSeconds(__sec);
        varyear    = now.getFullYear();
        varmonth   = now.getMonth() + 1;
        vardate    = now.getDate();
        varhours   = parseInt(__hour, 10);
        varminutes = parseInt(__min, 10);
        varseconds = parseInt(__sec, 10);

        varmonth_1  = new Array();
        month_1[0]  = "Jan";
        month_1[1]  = "Feb";
        month_1[2]  = "Mar";
        month_1[3]  = "Apr";
        month_1[4]  = "May";
        month_1[5]  = "Jun";
        month_1[6]  = "Jul";
        month_1[7]  = "Aug";
        month_1[8]  = "Sep";
        month_1[9]  = "Oct";
        month_1[10] = "Nov";
        month_1[11] = "Dec";

        vartimeValue = "";
        timeValue += ((date < 10) ? "0" : "") + date;

        timeValue += ((month < 10) ? " " : " ") + month_1[month - 1];
        timeValue += " " + year;

        timeValue += " ";
        if (hours == 0)
            timeValue += (12)
            else timeValue += ((hours > 12) ? hours - 12 : hours)
            timeValue += ((minutes < 10) ? ":0" : ":") + minutes
            timeValue += ((seconds < 10) ? ":0" : ":") + seconds
            timeValue += (hours >= 12) ? " P.M." : " A.M."
    },
    GetTimeString : function() { return this.TimeString; },
    CheckAudioOutStatus : function() { return this.audioOut; },
}