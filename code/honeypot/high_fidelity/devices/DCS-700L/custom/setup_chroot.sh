#! /firmadyne/sh

BUSYBOX=/firmadyne/busybox
NEW_ROOT=/honey_root/
# same date as the busybox binary
MODIFIED_DATE='2018-11-03 06:11:28'

# setup proper creds handling. The FW places creds in `/etc/passwd`. This is bad as the file can be read by anyone and the hash could thus be cracked.
cp /firmadyne/etc/group /etc/group
${BUSYBOX} chmod 644 /etc/group
cp /firmadyne/etc/passwd /etc/passwd
${BUSYBOX} chmod 644 /etc/passwd
cp /firmadyne/etc/shadow /etc/shadow
${BUSYBOX} chmod 600 /etc/shadow

# To prevent basic escaping from the `chroot` we make the logged-in user non-root and give `root` a complicated password.
# `chroot` escape with UID0: https://filippo.io/escaping-a-chroot-jail-slash-1/
#
# Yes, this is hard coded here. Yes, hard coded password are bad.
# But this `chroot` setup isn't meant to be secure, it's just a simple layer to hide `/firmadyne`. Adversaries will have to escape chroot to read this file. And if they do, they can see the `/firmadyne` dir which means they'll run from the honeypot anyway.
echo 'root:3E%AvR;[TrkMF1Rx-nh=i-iu65~T7Mn6' | ${BUSYBOX} chpasswd -c sha512
echo 'admin:admin' | ${BUSYBOX} chpasswd -c sha512

# set passwd modified date to amount of days
MODIFIED_DAYS_SINCE_0=$(${BUSYBOX} expr \( $(${BUSYBOX} date -u -d"${MODIFIED_DATE}" +%s) \) / 86400)
for LINE in $(cat /etc/shadow | ${BUSYBOX} cut -d":" -f 1-2)
do
    echo ${LINE}":${MODIFIED_DAYS_SINCE_0}::::::" >> /tmp/shadow.bak
done
${BUSYBOX} mv /tmp/shadow.bak /etc/shadow
${BUSYBOX} chmod 600 /etc/shadow

# prep chroot-ed file system
# Again, this `chroot` is not meant to be a security layer. We mount with `--bind` which means edits inside the chroot will reflect to outside. There likely is some way to abuse this to escape the chroot.
${BUSYBOX} mkdir -p /home/admin
for DIR in bin dev etc etc_ro home/admin lib lost+found media mnt mydlink proc root run sbin sys tmp usr var
do
    ${BUSYBOX} mkdir -p ${NEW_ROOT}${DIR}
    ${BUSYBOX} mount --bind --rprivate /${DIR} ${NEW_ROOT}${DIR}
done
# needed to spawn a shell with telnet
${BUSYBOX} mount --bind /dev/pts ${NEW_ROOT}/dev/pts
cd ${NEW_ROOT}
${BUSYBOX} ln -s bin/busybox init
cd -
# fix permissions
${BUSYBOX} chown admin:admin ${NEW_ROOT}/home/admin
${BUSYBOX} chmod 750 ${NEW_ROOT}/root/
${BUSYBOX} chmod 755 ${NEW_ROOT}/bin/ ${NEW_ROOT}/dev/ ${NEW_ROOT}/etc_ro/ ${NEW_ROOT}/mnt/ ${NEW_ROOT}/sbin/ ${NEW_ROOT}/usr/
${BUSYBOX} chmod 777 ${NEW_ROOT}/tmp/
${BUSYBOX} chmod +t ${NEW_ROOT}/tmp/

# remount `/proc` with `hidepid=2` to disallow non-root users to see processes of other users
${BUSYBOX} mount -o remount,nosuid,nodev,noexec,relatime,rw,hidepid=2 ${NEW_ROOT}/proc

# hide last hints of firmae/firmadyne and QEMU
${BUSYBOX} mount --bind -r /firmadyne/spoofed_system_info/proc/version /honey_root/proc/version
${BUSYBOX} mount --bind -r /firmadyne/spoofed_system_info/proc/cmdline /honey_root/proc/cmdline
${BUSYBOX} mount --bind -r /firmadyne/spoofed_system_info/proc/scsi/ /honey_root/proc/scsi/
# mapping individual files is a hassle due to long path. I'm also not sure I'd find every occurrence of QEMU in the device info files.
${BUSYBOX} rm /firmadyne/spoofed_system_info/sys/devices/pci0000:00/0000:00:0a.1/.gitkeep
${BUSYBOX} mount --bind -r /firmadyne/spoofed_system_info/sys/devices/pci0000:00/0000:00:0a.1 /honey_root/sys/devices/pci0000:00/0000:00:0a.1/
${BUSYBOX} mount --bind -r /firmadyne/spoofed_system_info/sys/block/sda/device/model /honey_root/sys/block/sda/device/model

# disallow non-root users to see dmesg, as it contains logs about firmadyne, etc.
${BUSYBOX} sysctl -w kernel.dmesg_restrict=1

# set modified time so timestamp in `ls -l` shows an old time. Otherwise all files have the date of emulation start.
FMT_MODIFIED_DATE=$(date -u -d"${MODIFIED_DATE}" +%Y%m%d%H%M.%S)
${BUSYBOX} find ${NEW_ROOT} -exec touch -h -t ${FMT_MODIFIED_DATE} {} +

${BUSYBOX} chroot ${NEW_ROOT} /usr/sbin/telnetd -p 23 -l /bin/login
