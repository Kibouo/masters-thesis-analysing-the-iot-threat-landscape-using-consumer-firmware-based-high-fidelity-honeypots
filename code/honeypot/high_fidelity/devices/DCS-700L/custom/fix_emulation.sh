#! /firmadyne/sh

BUSYBOX=/firmadyne/busybox

# args:
IOT_SUBNET_GATEWAY=${1}

# constrain IP to proper subnet
IP_SUB=$(${BUSYBOX} ip address | ${BUSYBOX} grep 'br0' | ${BUSYBOX} grep '/8' | ${BUSYBOX} cut -d ' ' -f 6)
IP=$(echo ${IP_SUB} | ${BUSYBOX} cut -d '/' -f 1)
${BUSYBOX} ip address add ${IP}/24 dev br0
# for some reason QEMU/the emulated FW locks up if removing the old IP is done too fast
(${BUSYBOX} sleep 180 && ${BUSYBOX} ip address delete ${IP_SUB} dev br0) &

# default gateway setup
${BUSYBOX} ip route delete default || true
${BUSYBOX} ip route add default via ${IOT_SUBNET_GATEWAY}

# MAC setup
# QEMU assigns incremental addresses starting at 52:54:00:12:34:56, which is too obvious.
# MAC address prefixes are vendor identifying. '1C:5F:2B' is a valid prefix for a D-Link device.
${BUSYBOX} ip link set dev eth0 address '1C:5F:2B:4F:45:91'
${BUSYBOX} ip link set dev eth1 address '1C:5F:2B:4F:45:92'
${BUSYBOX} ip link set dev eth2 address '1C:5F:2B:4F:45:93'
${BUSYBOX} ip link set dev eth3 address '1C:5F:2B:4F:45:94'

# DNS setup
echo 'nameserver 8.8.8.8' > /etc/resolv.conf
echo 'nameserver 8.8.4.4' >> /etc/resolv.conf

# replace FW's busybox with FirmAE's busybox, to give adversaries more tools to play with. Letting them download stuff over the highly limited bandwidth of the emulated network would potentially annoy them and decrease retention rate.
cp ${BUSYBOX} /bin/busybox
${BUSYBOX} ln -s /bin/busybox /bin/base64 # base64 is used a lot

# firmadyne's kernel is 4.1.17, but the original FW uses an older one. The `modules` directory contains a dir for this old version.
rm -rf /lib/modules/2.6.21/
mkdir -p /lib/modules/4.1.17/kernel

# Tweaks for realism
cp /etc_ro/web/home.htm /etc_ro/web/index.html
# camera feed usually tries to get `image.jpg` which triggers the web server to get a screenshot. Due to emulation this is obviously not possible, so we replace the image endpoint which is called and supply our own feed.
cp /firmadyne/camera_feed/patched_web/* /etc_ro/web/
/firmadyne/camera_feed/run.sh &

# some nvram are not loaded due to `/dev/gpio` being missing
for OVERRIDE in $(cat /firmadyne/libnvram_override_other)
do
    KEY=$(echo ${OVERRIDE} | ${BUSYBOX} sed 's/=.*//g')
    VAL=$(echo ${OVERRIDE} | ${BUSYBOX} sed 's/.*=//g')
    ${BUSYBOX} sed -i "s/%%${KEY};%%/${VAL}/g" /etc_ro/web/*.htm
    ${BUSYBOX} sed -i "s/%%${KEY};%%/${VAL}/g" /etc_ro/web/cgi/*.cgi
done

# there are no TLS certs on the device
# NOTE: despite certs working, the `curl` and `wget` available on the firmware support only up to TLSv1.1 🙃 That's something fun for the attacker to figure out.
mkdir -p /etc/pki/tls/certs/
cp /firmadyne/ca-bundle.crt /etc/pki/tls/certs/
${BUSYBOX} chmod 444 /etc/pki/tls/certs/ca-bundle.crt