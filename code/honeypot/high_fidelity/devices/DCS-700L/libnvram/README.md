# Building
```sh
wget https://musl.cc/mipsel-linux-musl-cross.tgz
tar xzf mipsel-linux-musl-cross.tgz
cd <path/to/libnvram/source>
make CC=<path/to/mipsel-linux-musl-cross/bin/mipsel-linux-musl-gcc>
```

## Sources
- https://ariya.io/2020/06/cross-compiling-with-musl-toolchains
- https://musl.cc/
