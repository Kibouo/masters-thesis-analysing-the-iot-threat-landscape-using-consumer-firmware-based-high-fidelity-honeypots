# DCS-700L
This is an old IP-camera without notable CVEs. This dockerfile is used for testing purposes.

## NVRAM
`libnvram`'s `nvram_get` had to be customised. The device takes an additional argument before the key and value.

## Firmware
- release date: 2016/02/12
- name: DCS-700L_A1_FW_v1.03.09.zip
- source: https://ftp.dlink.ru/pub/IP-Camera/DCS-700L/Firmware