#! /bin/bash

# Check whether the file is a capture part or not (due to max size rotation).
# Creation of parts does happen, despite the base name being accurate up to a second and size being several MB. This is because rotation due to hitting the file size maximum doesn't update the date. It keeps the original capture's date and just adds an index.
if [[ "${1}" == *.part ]]
then
    # ends on `.part` without an index. This is a full log.
    mv "${1}" "${1%.part}" # remove useless `.part`
    gzip -S .pcap.gz "${1%.part}"
else
    gzip -S .pcap.gz "${1}"
fi