#! /bin/sh

# NOTE: this only captures anything happening on the Docker interface (by definition). The host only forwards exposed ports to this interface. Implications of this are that only network to these exposed ports will be logged and not to closed ports, e.g. during an `nmap` scan. Nothing interesting should happen on a closed port however, so this should not matter.
tcpdump \
    -G 86400 `# rotate after 24h` \
    -i ${HONEYPOT_NETWORK_NAME} `# listen to IoT network interface` \
    -w ${LOG_DIR}tcpdump/$(date --utc '+%Y-%m-%d_%H:%M:%S.%z').part `# output file name. Additional "." for part nr.` \
    -C 200 `# max file size in MB. Auto-rotates when hit.` \
    -n `# no resolving. Keep raw data for analysis.` \
    -vv `# extra verbose capturing` \
    -s 0 `# default snapshotting. Don't drop or corrupt data.` \
    -Z ${LOG_USER} `# give ownership of logs to ourselves instead of to root (sudo)` \
    -z ${UTIL_PATH}/gzip_a_pcap.sh `# process files on rotate`