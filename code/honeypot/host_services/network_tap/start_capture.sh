#! /bin/sh

# Generates and sets up a `systemd` service to tcpdump.
# `tcpdump` requires `sudo` privs, but we want it to auto-restart on potential errors.

# Quit on errors
set -e

SERVICE_NAME=honeypot_tcpdump

# Stop service in case we're re-running this setup script
(sudo systemctl stop ${SERVICE_NAME}.service || true) 2>/dev/null

# Generate `.service` file dynamically. This is needed to expand env vars.
(echo "[Unit]
Description=Run tcpdump for the honeypot

[Service]
Restart=always
RestartSec=30
Environment=\"HONEYPOT_NETWORK_NAME=${HONEYPOT_NETWORK_NAME}\"
Environment=\"ORIG_PWD=${ORIG_PWD}\"
Environment=\"LOG_DIR=${LOG_DIR}\"
Environment=\"UTIL_PATH=$(pwd)\"
Environment=\"LOG_USER=${ORIG_USER}\"
ExecStart=$(pwd)/capture.sh" \
| sudo tee ${SERVICE_PATH}${SERVICE_NAME}.service) 1>/dev/null

# Start service
sudo systemctl daemon-reload
sudo systemctl start ${SERVICE_NAME}.service