# Network tap
Runs `tcpdump` and captures all data of the IoT network's Docker interface. Auto-rotates every set amount of hours or when file size exceeds a limit. Data is stored as `pcap`s and zipped.

This is not a Docker container for segregation reasons. We don't want to share resources with containers. Also, if for some reason the containers don't restart we can still capture incoming connection attempts, etc.