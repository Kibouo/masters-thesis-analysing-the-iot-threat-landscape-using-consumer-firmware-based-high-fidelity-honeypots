# Telnet Proxy
Telnet is the main entry point of the honeypot. Using a proxy allows for:
- prevent QEMU/the virtualised device to be overloaded and going down
- honeytokens (multiple credentials)
- logging of shell interactions