#! /bin/bash

# Generates and sets up a `systemd` service for the telnet proxy.
# systemd provides the restart-on-crash for us.

# Quit on errors
set -e

SERVICE_NAME=telnet_proxy

# Stop service in case we're re-running this setup script
(sudo systemctl stop ${SERVICE_NAME}.service || true) 2>/dev/null

COWRIE_DIR=`pwd`/cowrie/
(cat ./${SERVICE_NAME}.service.template              \
| sed "s/<user>/${ORIG_USER}/g"                               \
| sed "s~<work_dir>~${COWRIE_DIR}~g"                            \
| sudo tee ${SERVICE_PATH}${SERVICE_NAME}.service) 1>/dev/null

# Start service
sudo systemctl daemon-reload
sudo systemctl start ${SERVICE_NAME}.service