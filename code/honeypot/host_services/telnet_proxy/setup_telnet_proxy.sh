#! /bin/bash

# Quit on errors
set -e

# Stop service in case we're re-running this setup script
SERVICE_NAME=telnet_proxy
(sudo systemctl stop ${SERVICE_NAME}.service || true) 2>/dev/null

COWRIE_LOG_DIR=${LOG_DIR}telnet_proxy/
mkdir -p ${COWRIE_LOG_DIR}downloads/
mkdir -p ${COWRIE_LOG_DIR}tty/

git clone http://github.com/cowrie/cowrie || true
cd cowrie

COWRIE_ENV=cowrie-env
virtualenv --python=python3.7 ${COWRIE_ENV}
source ${COWRIE_ENV}/bin/activate
# NOTE: try bellow in case of `pip` error: `ModuleNotFoundError: No module named 'pip._internal.utils'`
# curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
# python3 get-pip.py --force-reinstall
pip install --upgrade pip
pip install --upgrade -r requirements.txt

COWRIE_DIR=`pwd`/
cd ../config/
./gen_config.sh ${COWRIE_LOG_DIR} ${COWRIE_DIR}
cd -
