#! /bin/bash

# Quit on errors
set -e

TMP=`mktemp`

# args:
LOG_DIR=${1}
COWRIE_DIR=${2}

cat ./cowrie.cfg.template                               \
| sed "s~<log_dir>~${LOG_DIR}~g"                        \
| sed "s/<backend_port>/${IPCAM_LOCAL_TELNET_PORT}/g"   \
| sed "s/<listen_port>/${TELNET_PROXY_LOCAL_PORT}/g"    \
| sed "s/<hostname>/`hostname`/g"                       \
> ${TMP}

mv ${TMP} ${COWRIE_DIR}etc/cowrie.cfg
cp ./userdb.txt ${COWRIE_DIR}etc/userdb.txt