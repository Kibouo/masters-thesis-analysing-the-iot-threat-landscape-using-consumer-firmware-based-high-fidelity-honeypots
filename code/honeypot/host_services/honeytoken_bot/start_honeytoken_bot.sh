#! /bin/sh

# Generates and sets up a `systemd` service to generate honeytokens, add them word telnet proxy's wordlist, and distribute them.
# `honeytoken_bot.sh` is not simply called in the shell as it needs to run periodically.

# Quit on errors
set -e

SERVICE_NAME=honeytoken_bot

# Stop service in case we're re-running this setup script
(sudo systemctl stop ${SERVICE_NAME}.timer || true) 2>/dev/null

# Generate `.service` file dynamically. This is needed to expand env vars.
COWRIE_WL=`pwd`/../telnet_proxy/cowrie/etc/userdb.txt
(cat ./${SERVICE_NAME}.service.template             \
| sed "s~<cowrie_wl>~${COWRIE_WL}~g"            \
| sed "s~<pwd>~`pwd`~g"                             \
| sed "s~<user>~`whoami`~g"                             \
| sed "s~<log_dir>~${LOG_DIR}~g"            \
| sed "s~<internet_if_ip>~${INTERNET_IF_IP}~g"       \
| sudo tee ${SERVICE_PATH}${SERVICE_NAME}.service) 1>/dev/null
sudo cp ./${SERVICE_NAME}.timer ${SERVICE_PATH}

# Start service
sudo systemctl daemon-reload
sudo systemctl start ${SERVICE_NAME}.timer