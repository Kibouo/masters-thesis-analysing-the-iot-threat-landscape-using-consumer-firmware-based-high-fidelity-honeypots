#! /bin/bash

# params
## env
# - COWRIE_WORDLIST
# - INTERNET_IF_IP
# - LOG_DIR
## static
NAME=exposure_report.json
TEMPLATE=./honeytoken.json.template
COOKIE_JAR=/tmp/cookie

function generate_password()
{
    # generate a random password
    local length=8
    local pwd_charset="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz!@$&"
    local password=""
    while [ ${#password} -lt $length ]; do
        password="${password}${pwd_charset:$(( $RANDOM % ${#pwd_charset} )):1}"
    done
    echo "${password}"
}

function save_password()
{
    echo ${1} ${2} >> ${LOG_DIR}/honeytokens
    echo '/^.*admin$/:x:'"${1}" >> ${COWRIE_WORDLIST}
}


# Pastebin
# an API is available but requires logging in
PASSWORD=$(generate_password)
CONTENT=$(cat ${TEMPLATE} | sed "s/<password>/${PASSWORD}/g" | sed "s/<ip>/${INTERNET_IF_IP}/g" | sed "s/<time>/$(date +%s)/g")
HIDDEN_CSRF=$(curl -s --cookie-jar ${COOKIE_JAR} https://pastebin.com | grep 'csrf-token' | cut -d ' ' -f 3 | cut -d '"' -f 2)
LOCATION=$(curl -v --cookie ${COOKIE_JAR} \
    --form "_csrf-frontend=${HIDDEN_CSRF}" \
    --form "PostForm[text]=${CONTENT}" \
    --form 'PostForm[format]=255' \
    --form 'PostForm[expiration]=N' \
    --form 'PostForm[status]=0' \
    --form 'PostForm[is_password_enabled]=0' \
    --form 'PostForm[is_burn]=0' \
    --form "PostForm[name]=${NAME}" \
    https://pastebin.com 2> >(grep 'location: https://pastebin.com/' | cut -d' ' -f 3))
if [[ "${LOCATION}" != '' ]]; then
    echo "Posted ${PASSWORD} to Pastebin"
    save_password ${PASSWORD} ${LOCATION}
fi

# Hastebin
PASSWORD=$(generate_password)
CONTENT=$(cat ${TEMPLATE} | sed "s/<password>/${PASSWORD}/g" | sed "s/<ip>/${INTERNET_IF_IP}/g" | sed "s/<time>/$(date +%s)/g")
LOCATION=$(curl -s -d "${CONTENT}" -L http://hastebin.com/documents | grep '\{"key":"' | cut -d'"' -f 4)
if [[ "${LOCATION}" != '' ]]; then
    echo "Posted ${PASSWORD} to Hastebin"
    save_password ${PASSWORD} "http://hastebin.com/${LOCATION}"
fi

# Controlc
PASSWORD=$(generate_password)
CONTENT=$(cat ${TEMPLATE} | sed "s/<password>/${PASSWORD}/g" | sed "s/<ip>/${INTERNET_IF_IP}/g" | sed "s/<time>/$(date +%s)/g" | sed -z 's/\n/%0D%0A/g' | sed 's/ /%20/g' )
TIMESTAMP=$(curl -s --cookie-jar ${COOKIE_JAR} https://controlc.com/ | grep 'timestamp' | cut -d ' ' -f 4 | cut -d "'" -f 2)
LOCATION=$(curl -s --cookie ${COOKIE_JAR} \
    -H 'Upgrade-Insecure-Requests: 1' \
    -H 'Referer: https://controlc.com/' \
    -d "subdomain=&antispam=1&website=&paste_title=${NAME}&input_text=${CONTENT}&timestamp=${TIMESTAMP}&paste_password=&code=1" \
    'https://controlc.com/index.php?act=submit' | grep 'Paste submitted successfully!' | cut -d'"' -f 2)
if [[ "${LOCATION}" != '' ]]; then
    echo "Posted ${PASSWORD} to Controlc"
    save_password ${PASSWORD} ${LOCATION}
fi

# pastie.org
PASSWORD=$(generate_password)
CONTENT=$(cat ${TEMPLATE} | sed "s/<password>/${PASSWORD}/g" | sed "s/<ip>/${INTERNET_IF_IP}/g" | sed "s/<time>/$(date +%s)/g")
LOCATION=$(curl -s -d 'language=json' --data-urlencode "content=${CONTENT}" http://pastie.org/pastes/create | grep 'Found. Redirecting to' | cut -d' ' -f 4)
if [[ "${LOCATION}" != '' ]]; then
    echo "Posted ${PASSWORD} to pastie.org"
    save_password ${PASSWORD} "http://pastie.org${LOCATION}"
fi