#! /bin/sh

# Generates and sets up a `systemd` service to healthcheck services hosted with docker. See `README.md` for more info.
# `healthcheck.sh` is not simply called in the shell as docker requires `sudo` privileges.

# Quit on errors
set -e

SERVICE_NAME=healthcheck

# Stop service in case we're re-running this setup script
(sudo systemctl stop ${SERVICE_NAME}.timer || true) 2>/dev/null

# Generate `.service` file dynamically. This is needed to expand env vars.
(echo "[Unit]
Description=IoT emulation healthcheck

[Service]
Environment=\"DOCKER_COMPOSE_DIR=${ORIG_PWD}\"
Environment=\"SERVICES_TO_HEALTH_CHECK=${SERVICES_TO_HEALTH_CHECK}\"
Environment=\"TCPDUMP_LOG_DIR=${LOG_DIR}tcpdump\"
Environment=\"GZIP_A_PCAP=$(pwd)/../network_tap/gzip_a_pcap.sh\"
ExecStart=$(pwd)/healthcheck.sh" \
| sudo tee ${SERVICE_PATH}${SERVICE_NAME}.service) 1>/dev/null
sudo cp ./${SERVICE_NAME}.timer ${SERVICE_PATH}

# Start service
sudo systemctl daemon-reload
sudo systemctl start ${SERVICE_NAME}.timer