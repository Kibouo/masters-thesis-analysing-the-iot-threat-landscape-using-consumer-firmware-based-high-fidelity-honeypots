# Docker healthcheck
The high-fidelity docker containers redirect all traffic from their `eth0` to their QEMU's `tap`'s. The container's networking breaks due to this, as all responses are sent to the tap. It has no internet and can't even nmap itself. Thus, the docker container can't run its own health check to see if the supposed services/ports are up.

This is run on the host and not in a docker container as that'd require the healthcheck container to be on the IoT network. We don't want to place extra stuff for adversaries to see and become suspicious about.
