#! /bin/bash

function healthcheck_device() {
    # args
    CONTAINER_NAME=${1}

    # TCP
    IP_PORTS=`docker container ls --format 'table {{.Names}}\t{{.Ports}}' | grep ${CONTAINER_NAME} | sed 's/,/\n/g' | grep tcp | sed 's/->.*//g' | sed 's/.*\s//g' | uniq`
    for ip_port in ${IP_PORTS}
    do
        ip=`echo ${ip_port} | cut -d':' -f 1`
        port=`echo ${ip_port} | cut -d':' -f 2`
        nmap ${ip} -p${port} -sT | grep -E "${port}.*open" >/dev/null
        if [[ $? -ne 0 ]]
        then
            return 1
        fi
    done

    # UDP
    IP_PORTS=`docker container ls --format 'table {{.Names}}\t{{.Ports}}' | grep ${CONTAINER_NAME} | sed 's/,/\n/g' | grep udp | sed 's/->.*//g' | sed 's/.*\s//g' | uniq`
    for ip_port in ${IP_PORTS}
    do
        ip=`echo ${ip_port} | cut -d':' -f 1`
        port=`echo ${ip_port} | cut -d':' -f 2`
        nmap ${ip} -p${port} -sU | grep -E "${port}.*open" >/dev/null
        if [[ $? -ne 0 ]]
        then
            return 1
        fi
    done
    return 0
}

for device in `echo -n ${SERVICES_TO_HEALTH_CHECK} | sed 's/,/\n/g'`
do
    healthcheck_device ${device}
    if [[ $? -ne 0 ]]
    then
        # rotate `tcpdump`. Usually it does this on its own but we will force restart
        systemctl stop honeypot_tcpdump.service # prevent modification of files while we backup
        find ${TCPDUMP_LOG_DIR} -type f ! -name "*.gz" -exec ${GZIP_A_PCAP} {} \;
        # rotate the rest
        systemctl start log_handling.service
        sleep 20

        # We don't know why something failed, it might be because an attacker messed up something on the file system. To do a full "clean" restart of a docker service, the container has to be removed and recreated from an image. This means we gotta take it all down.
        systemctl restart haas.service
        exit
    fi
done