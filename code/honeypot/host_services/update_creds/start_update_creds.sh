#! /bin/bash

# Generates and sets up a `systemd` service to update telnet proxy credentials.
# systemd provides the periodic running for us.

# Quit on errors
set -e

SERVICE_NAME=update_creds

# Stop service in case we're re-running this setup script
(sudo systemctl stop ${SERVICE_NAME}.timer || true) 2>/dev/null

COWRIE_DIR=`pwd`/../telnet_proxy/cowrie/
(cat ./${SERVICE_NAME}.service.template              \
| sed "s~<pwd>~`pwd`~g"                            \
| sed "s~<cowrie_dir>~${COWRIE_DIR}~g"                            \
| sed "s~<log_dir>~${LOG_DIR}~g"                                \
| sudo tee ${SERVICE_PATH}${SERVICE_NAME}.service) 1>/dev/null
sudo cp ./${SERVICE_NAME}.timer ${SERVICE_PATH}

# Start service
sudo systemctl daemon-reload
sudo systemctl start ${SERVICE_NAME}.timer