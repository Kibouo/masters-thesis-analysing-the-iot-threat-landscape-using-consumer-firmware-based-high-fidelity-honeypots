#! /bin/bash

# args:
# LOG_DIR=${1}
# COWRIE_DIR=${2}

LOG_PASS=`mktemp`
cat ${LOG_DIR}telnet_proxy/cowrie.json | grep 'password":".*?"' -P --only-matching | cut -d':' -f 2 | sed -E 's/^"(\\u.{4})*|"$//g' | sort | uniq > ${LOG_PASS}

USERDB_PASS=`mktemp`
cat ${COWRIE_DIR}etc/userdb.txt | grep -v '^#' | cut -d':' -f 3 | sort | uniq > ${USERDB_PASS}

IFS=$'\n'
for pass in $(comm -13 ${USERDB_PASS} ${LOG_PASS})
do
    if [ "${pass}" != '\n' ] && [ "${pass}" != '' ]
    then
        echo '/^.*admin$/:x:'"${pass}" >> ${COWRIE_DIR}etc/userdb.txt
    fi
done

rm -f ${LOG_PASS} ${USERDB_PASS}
systemctl restart telnet_proxy.service