# Update telnet credentials
Allowing everything is suspicious during manual interactions. However, we also want to allow as many bots as possible to gather info from them. At the same time, we compared passwords from early logs with [word lists](https://github.com/danielmiessler/SecLists/tree/master/Passwords), and found that the overlap is not *too* good. This is likely because the word lists are for generic PC/servers, not IoT.

This service periodically scans for passwords in the logs, and adds them to the user db. This allows those passwords from that moment.