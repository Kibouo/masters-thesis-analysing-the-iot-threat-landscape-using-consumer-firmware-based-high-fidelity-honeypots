#! /bin/bash

# Periodically rotates all logs. Except `tcpdump` as that has proper log handling built-in.
# Log rotation is done manually instead of with `logrotate` as each application handles logging differently and non-conforming. E.g. cowrie: https://github.com/cowrie/cowrie/issues/1462

# Quit on errors
set -e

SERVICE_NAME=log_handling

# Stop service in case we're re-running this setup script
(sudo systemctl stop ${SERVICE_NAME}.timer || true) 2>/dev/null

# Generate `.service` file dynamically. This is needed to expand env vars.
(cat ./${SERVICE_NAME}.service.template             \
| sed "s~<log_dir>~${LOG_DIR}~g"            \
| sed "s~<pwd>~`pwd`~g"                             \
| sed "s/<user>/${ORIG_USER}/g"                         \
| sudo tee ${SERVICE_PATH}${SERVICE_NAME}.service) 1>/dev/null
sudo cp ./${SERVICE_NAME}.timer ${SERVICE_PATH}

# Start service
sudo systemctl daemon-reload
sudo systemctl start ${SERVICE_NAME}.timer