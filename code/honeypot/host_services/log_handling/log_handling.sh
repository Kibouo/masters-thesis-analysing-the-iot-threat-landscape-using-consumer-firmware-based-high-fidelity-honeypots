#! /bin/bash

# Does:
# 1. gzips files, removing originals
# 2. touch original file names to not upset applications
# 3. sends to backup server

# Rotation compresses all logs from the past day. So let's fix time stamps.
# Also makes sure we match with `tcpdump`'s timestamps
DATE=$(date --utc '+%Y-%m-%d_%H:%M:%S.%z' -d '-1 day')

cd ${LOG_DIR}

## telnet proxy
cd ./telnet_proxy/tty/
# wipe useless log files
rm ./telnet-????????-*.log
for FILE in $(find ./ -maxdepth 1 -type f)
do
    # 12 chars are enough to identify a session
    mv ${FILE} ${FILE:0:12}
    gzip -S .${DATE}.gz ${FILE:0:12}
    # cowrie creates tty files on demand
    # touch
done
cd -

cd ./telnet_proxy/
# cowrie already rotates for us
for FILE in $(find ./ -maxdepth 1 -type f -name 'cowrie.json.????-??-??')
do
    gzip -S .${DATE}.gz ${FILE:0:24}
done
for FILE in $(find ./ -maxdepth 1 -type f -name 'cowrie.log.????-??-??')
do
    gzip -S .${DATE}.gz ${FILE:0:23}
done
cd -

# `tcpdump` does its own rotation
# cd ./tcpdump/

cd ./firewall/
FILE=iptables.log
# rsyslog is used for this logging. It can't handle the file being removed and recreated with the same name. So we just clear it after backing up.
gzip -kS .${DATE}.gz ${FILE}
echo -n '' > ${FILE}
cd -

cd ./mqtt/hub/
FILE=mosquitto.log
# rsyslog is used for this logging. It can't handle the file being removed and recreated with the same name. So we just clear it after backing up.
gzip -kS .${DATE}.gz ${FILE}
echo -n '' > ${FILE}
cd -

cd ./mqtt/traffic_gen/
for FILE in $(find ./ -maxdepth 1 -type f)
do
    gzip -S .${DATE}.gz ${FILE}
    touch ${FILE}
done
cd -