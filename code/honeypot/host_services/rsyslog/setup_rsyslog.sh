#! /bin/bash

# fix permissions for rsyslog to be able to write in the default user's home directory.
(cat ./rsyslog.conf.template \
| sed "s/<user>/`whoami`/g" \
| sudo tee /etc/rsyslog.conf) 1>/dev/null