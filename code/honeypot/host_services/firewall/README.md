# Firewall
Configures firewall rules of the honeypot host and what is being passed to Docker. It blocks incoming known web scrapers/scanners, e.g. Shodan, and limits the incoming bandwidth. Also heavily rate limits outgoing traffic to limit the contribution to malicious activities.

Outgoing traffic is not fully blocked for 2 reasons:
1. choosing the ports to block is hard. If ports 80 and 445 are blocked to prevent a DoS, maybe the attacker can't download a stage of their exploit.
2. completely blocking things might alarm an adversary, while a slow connection to an IoT device is not unusual.

The blocking of scanners is dynamic using a `systemd` service.

The firewall works on a host-level, instead of in individual Docker containers. This is because:
- it prevents alteration of rules by adversaries who gained access to the container.
- usage of `iptables` requires running containers with `--privileged`, which is highly discouraged.
- it allows centralised management of the rules in case of updates.