#! /bin/sh

# Quit on errors
set -e

FIREWALL_LOG_DIR=${LOG_DIR}firewall/
mkdir -p ${FIREWALL_LOG_DIR}

# Config rsyslog to output `iptables` logs to a custom file
# https://askubuntu.com/questions/348439/where-can-i-find-the-iptables-log-file-and-how-can-i-change-its-location#348448
(echo ":msg,contains,\"[${IPTABLES_LOG_PREFIX}] \" -${FIREWALL_LOG_DIR}iptables.log
& stop" \
| sudo tee /etc/rsyslog.d/00-iptables.conf) 1>/dev/null

# Apply new config
sudo systemctl restart rsyslog