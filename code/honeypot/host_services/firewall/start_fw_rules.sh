#! /bin/sh

# NOTE: rules on the INPUT & OUTPUT tables do not affect Docker. This is because it uses the host as a gateway, and routed traffic doesn't pass these tables. Instead, rules have to be applied separately to the `DOCKER-USER` chain.
# https://docs.docker.com/network/iptables/#add-iptables-policies-before-dockers-rules

# NOTE: `-I <chain> <index>` sets the rule to be the `index`-th rule (1-indexed). This is needed to precede the default `RETURN` rule in the `DOCKER-USER` chain.

# NOTE: no attempt is made to drop `INVALID` packets as these might be part of an exploit we want to capture.

# NOTE: multiple uses of `--match` in 1 rules are evaluated from L -> R.
# https://serverfault.com/questions/1021947/iptables-with-hashlimit-and-state-new-blocks-too-many-new-connections

# Quit on errors
set -e

# Clear rules to prevent duplicates
sudo iptables -F
sudo iptables -t nat -F
# Let Docker add it's own stuff
sudo systemctl restart docker

### Host config
# Block all externally initiated internet traffic to the host
# NOTE: Docker manages its own port expose rules!
sudo iptables -A INPUT --in-interface ${INTERNET_IF_NAME} --match state --state NEW --jump DROP
## Exceptions:
# Don't block the telnet proxy.
# rewrite its destination
sudo iptables -I PREROUTING -t nat --in-interface ${INTERNET_IF_NAME} --protocol tcp --destination-port 23 --jump DNAT --to 127.0.0.1:${TELNET_PROXY_LOCAL_PORT}
# add an exception for the edited <ip>:<port> values, as that's what this rule sees
# https://stackoverflow.com/questions/12945233/iptables-forward-and-input
sudo iptables -I INPUT --in-interface ${INTERNET_IF_NAME} --protocol tcp --destination 127.0.0.1 --destination-port ${TELNET_PROXY_LOCAL_PORT} --jump ACCEPT
# we also need to allow localhost redirection for the above
# https://serverfault.com/questions/211536/iptables-port-redirect-not-working-for-localhost
sudo sysctl -w net.ipv4.conf.all.route_localnet=1
# Don't block SSH
# (sudo ipset restore -f ${IP_WHITELIST_FILE} || sudo ipset create ${IP_WHITELIST_NAME} hash:ip || true) 2>/dev/null
# sudo iptables -I INPUT --in-interface ${INTERNET_IF_NAME} --protocol tcp --destination-port 22 --match set --match-set ${IP_WHITELIST_NAME} src --jump ACCEPT
# NOTE: home IPs might change so we can't work with the whitelist.
sudo iptables -I INPUT --in-interface ${INTERNET_IF_NAME} --protocol tcp --destination-port 22 --jump ACCEPT

### Docker config
## Incoming traffic
# Load blacklist or create new list if needed
(sudo ipset restore -f ${IP_BLACKLIST_FILE} || sudo ipset create ${IP_BLACKLIST_NAME} hash:ip || true) 2>/dev/null
# Block specific IPs. This filters known scanners and potential poisoning adversaries.
# The set of IPs can be updated without touching this rule.
sudo iptables -I DOCKER-USER 1 --match set --match-set ${IP_BLACKLIST_NAME} src --jump DROP
# Enable logging
# Log for everything that did get through the IP filter. Otherwise we'll log blocked stuff too.
sudo iptables -I DOCKER-USER 2 --in-interface=${INTERNET_IF_NAME} --match limit --limit 10/min --jump LOG --log-prefix="[${IPTABLES_LOG_PREFIX}] "
# Rate limit bandwidth for incoming traffic
sudo iptables -I DOCKER-USER 3 --in-interface=${INTERNET_IF_NAME} --match hashlimit --hashlimit-name IOT_IN_BANDWIDTH --hashlimit-mode srcip,srcport --hashlimit-above 512kb/sec --jump DROP
# allow access to exposed Docker ports and block the rest
# --default handled by Docker

## Outgoing traffic
# Rate limit speed for established connections. This prevents the rate limiting of downloads due to TCP's management packets being limited too heavily.
# The rate limiting on established connections is now less strict as the following rules. However, this is not a huge problem. Connections are rarely kept alive during a high-volume spam attack. So the only thing it allows is for slightly faster uploads of big files and a faster tunnel connection.
sudo iptables -I DOCKER-USER 4 --out-interface=${INTERNET_IF_NAME} --match state --state ESTABLISHED,RELATED --match hashlimit --hashlimit-name IOT_STATE_OUT_BANDWIDTH --hashlimit-mode srcip,srcport --hashlimit-upto 1kb/sec --jump ACCEPT
# Rate limit speed for new connections. This is per service, per IoT device.
# Exception for the web server, as the images (video feed and HTML) are usually large. We don't want an adversary to leave simply because the webpage loads too slow. This rule is per destination IP, as multiple adversaries could be trying to enumerate the service.
# Abuse is contained due to the port restriction.
sudo iptables -I DOCKER-USER 5 --out-interface=${INTERNET_IF_NAME} --protocol tcp --source-port 80 --match hashlimit --hashlimit-name IOT_NEW_OUT_HTTP_BANDWIDTH --hashlimit-mode dstip --hashlimit-upto 32kb/sec --jump ACCEPT
# The size was chosen such that e.g. shell interactions and simple webpages feel slow (but are not unusable), while reflection attacks' impact is limited. Reflected packets can be as small as [400B](https://blog.cloudflare.com/reflections-on-reflections/).
sudo iptables -I DOCKER-USER 6 --out-interface=${INTERNET_IF_NAME} --match hashlimit --hashlimit-name IOT_NEW_OUT_BANDWIDTH --hashlimit-mode srcip,srcport --hashlimit-above 256b/sec --jump DROP
