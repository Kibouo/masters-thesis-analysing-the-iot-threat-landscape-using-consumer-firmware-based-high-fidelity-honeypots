#! /bin/sh

# Based on: https://github.com/krsmanovic/block-shodan-stretchoid
# Filter expanded with: https://github.com/aau-network-security/riotpot#12-Noise-Filter
for IP in $(cat ${LOG_DIR}firewall/iptables.log | awk '{print $11}' | grep SRC | sed 's/SRC=//')
do
    DOMAIN=$(dig -x ${IP} +short 2>/dev/null)
    if echo ${DOMAIN} | egrep -i "(^.*?(shodan|stretchoid|shadowserver|ezotech|alphastrike|censys|onyphe|binaryedge|caacbook|onlineprism|internet-census|netsystemsresearch|scan|rwth-aachen|leakix|rapid7|quadmetrics|ipip|arbor-observatory|criminalip|bitsight|natlas).*?$)" > /dev/null
    then
        ipset add ${IP_BLACKLIST_NAME} ${IP}
    fi
done