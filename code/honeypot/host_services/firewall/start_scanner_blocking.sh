#! /bin/sh

# Generates and sets up a `systemd` service to automatically block known scanners.
# `scanner_blocking.sh` is not simply called in the shell as `ipset` requires `sudo` privileges.

# Quit on errors
set -e

SERVICE_NAME=scanner_blocking

# Stop service in case we're re-running this setup script
(sudo systemctl stop ${SERVICE_NAME}.timer || true) 2>/dev/null

# Generate `.service` file dynamically. This is needed to expand env vars.
(echo "[Unit]
Description=IP-block web scanners

[Service]
Restart=on-failure
RestartSec=5s

Environment=\"ORIG_PWD=${ORIG_PWD}\"
Environment=\"LOG_DIR=${LOG_DIR}\"
Environment=\"IP_BLACKLIST_NAME=${IP_BLACKLIST_NAME}\"
ExecStart=$(pwd)/scanner_blocking.sh
ExecStop=$(which ipset) save ${IP_BLACKLIST_NAME} -f $(pwd)/${IP_BLACKLIST_FILE}" \
| sudo tee ${SERVICE_PATH}${SERVICE_NAME}.service) 1>/dev/null
sudo cp ./${SERVICE_NAME}.timer ${SERVICE_PATH}

# Start service
sudo systemctl daemon-reload
sudo systemctl start ${SERVICE_NAME}.timer