# Abuse check
Preventing abuse by limiting bandwidth with the firewall works. However, in case of e.g. a DoS, the data is still generated in the honeypot network and thus captured by `tcpdump`. This causes huge log files being created, despite most of the traffic being blocked. This is exclusively garbage UDP traffic which needlessly fills up our disk space.

This service frequently checks for abuse by checking for big `tcpdump` log files generated in succession. It then cleans the useless log files as well as clean restarts the honeypot to get rid of the malware.