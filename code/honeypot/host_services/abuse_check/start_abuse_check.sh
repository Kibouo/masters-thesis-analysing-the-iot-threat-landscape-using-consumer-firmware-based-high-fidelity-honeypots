#! /bin/sh

# Generates and sets up a `systemd` service to check for abuse based on `tcpdump` logs. See `README.md` for more info.
# `abuse_check.sh` is not simply called in the shell as docker requires `sudo` privileges.

# Quit on errors
set -e

SERVICE_NAME=abuse_check

# Stop service in case we're re-running this setup script
(sudo systemctl stop ${SERVICE_NAME}.timer || true) 2>/dev/null

# Generate `.service` file dynamically. This is needed to expand env vars.
(cat ./${SERVICE_NAME}.service.template             \
| sed "s~<tcpdump_log_dir>~${LOG_DIR}tcpdump~g"            \
| sed "s~<pwd>~`pwd`~g"                             \
| sudo tee ${SERVICE_PATH}${SERVICE_NAME}.service) 1>/dev/null
sudo cp ./${SERVICE_NAME}.timer ${SERVICE_PATH}

# Start service
sudo systemctl daemon-reload
sudo systemctl start ${SERVICE_NAME}.timer