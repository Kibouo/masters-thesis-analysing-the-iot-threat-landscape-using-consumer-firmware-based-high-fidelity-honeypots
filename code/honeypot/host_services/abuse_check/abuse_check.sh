#! /bin/bash

AMOUNT_BIG_FILES=$(find ${TCPDUMP_LOG_DIR} -type f -size +190M -newermt "`date -d '-20 min'`" | wc -l)

if [[ $AMOUNT_BIG_FILES -gt 0 ]]
then
    # rotate `tcpdump`. Usually it does this on its own but we will force restart
    systemctl stop honeypot_tcpdump.service # prevent modification of files while we backup
    find ${TCPDUMP_LOG_DIR} -type f ! -name "*.gz" -exec ${GZIP_A_PCAP} {} \;
    # rotate the rest
    systemctl start log_handling.service
    sleep 20

    # need to clean restart to wipe the malware away
    systemctl restart haas.service
    exit
fi
