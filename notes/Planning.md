# Planning
## Initial
- 20-09-2021 tot 17-10-2021 (~4 weken): research
    - basis kennis aanvullen
    - papers lezen
    - talks kijken
    - bestaande projecten bekijken
    - ...
- 18-10-2021 tot 14-11-2021 (~4 weken): experimenteren
    - testen emulators
    - honeypot setup
        - endpoints
        - communicatie
    - testen logging
    - ...
- 15-11-2021 tot 12-12-2021 (~4 weken): honeypot deployment-ready maken
- 13-12-2021 tot 19-12-2021 (~1 week): credentials voorbereiden
- 13-12-2021 tot 19-12-2021 (~1 week): deploy production
    - zowel honeypot als credentials
    - onvermijdelijke bugs fixen
- week van 20-12-2021: deploy honeypot
- 03-01-2022 tot 23-01-2022 (~3 weken): thesis text bijvullen (terwijl we wachten op eerste resultaten)
- 24-01-2022 tot 31-03-2022 (~9,5 weken): analyseren resultaten
    - verwerken
        - classificeren van aanvallen
            - [[homology diagram]]
        - fingerprinten van aanvallers m.b.v. verspreidde credentials
    - in thesis text opnemen
- 01-04-2022 tot 01-05-2022 (~4 weken): thesis text afwerken

## Half-year update
![](assets/2021-12-14-16-17-36.png)