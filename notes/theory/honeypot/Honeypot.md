# Honeypot
- resource
    - e.g.
        - service
        - application
        - system(s)
        - info/data
- intentionally invite "unauthorized" usage
    - e.g.
        - probed
        - attacked
        - compromised
        - used
        - accessed
- assume "user" == suspicious
- interactions
    - monitored
    - analysed
- *honeynet*
    - network of honeypots
- goal
    - monitor internet background noise
    - learn compromised nodes
    - identify new attacks
        - exploits & vulnerabilities
        - malware
        - adversary behaviour

## Requirements
- believable
    - mimic production
- no normal system interference
- log interactions

## Type
### Resource exploited
- [[Server-side honeypot]]
- [[Client-side honeypot]]
- [[Honeytoken]]

### [[Level of interaction]]

## [[Hardware vs software honeypot]]

## [[Implementation]]

## [[Code & Projects]]

## Source
- [[Sources|ENISA: Proactive detection of security incidents II - Honeypots]]
- [[Sources|Honeypots: Tracking Hackers]]
