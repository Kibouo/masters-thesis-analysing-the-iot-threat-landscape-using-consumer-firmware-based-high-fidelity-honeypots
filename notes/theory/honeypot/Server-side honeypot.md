# Server-side Honeypot
- simulate network service
    - i.e. server application
- passive
    - wait for adversary
    - adversary does the work
- goal
    - analyse
        - scans
            - auto & manual
        - interactions
            - post-intrusion

## [[Placement]]
- [[info]]

## Source
- [[Sources|ENISA: Proactive detection of security incidents II - Honeypots]]
- [[Sources|Honeypots: Tracking Hackers]]