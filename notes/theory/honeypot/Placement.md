# Placement
## [[Server-side honeypot]]
- goal of honeypot?
- location: not public as research team
    - [[detection prevention]]
- ![](../../assets/2021-09-29-17-29-13.png)
    - external (A) placement always bad
        - no [[data control]]

### Prevention
- goal
    - deceive & deter
        - waste time
- where
    - DMZ (B)
    - internal (C)

### Detection
- goal
    - detect attack & unauthorized access
        - insider threat
        - internal infection
            - e.g. malware by e-mail
- where
    - DMZ (B)
    - internal (C)

### Response
- goal
    - how does successful infection look?
        - mirror production
- where
    - same as production
    - DMZ (B)
        - most attacks
    - ~ internal (C)
        - less frequently compromised

### Research
- goal
    - inform on threats
- where
    - interested threat based
        - external threat
            - separate VLAN (D)
                - due to [[data control]]
                    - isolate from production
                    - not DMZ (B)
                        - access other servers
        - internal threat
            - internal (C) **but** with extra [[data control]]
                - e.g. L2/L3 firewall, IDS

## Source
- [[Sources|Honeypots: Tracking Hackers]]
