# Low-interaction Honeypot
- emulate resources
    - limited functionality vs. real
    - accuracy < 100%
        - emulated != real behaviour
        - affects interaction

## Pros
- low effort
    - deploy
    - maintain
- control possible interactions
    - e.g. attack, infection process
    - low risk actual compromise
    - see [[data control]]

## Cons
- no 0-day detection
    - can't emulate unknowns...
- high implementation cost
    - attacks follow specific steps
    - imbalance implementation
        - early steps: dense impl
        - later steps: sparse impl
- low accuracy
    - alert adversary
        - i.e. detect fake
        - automated scans
        - discrepancies during manual interaction
    - lack features
        - possible attack stops early

## Source
- [[Sources|ENISA: Proactive detection of security incidents II - Honeypots]]
- [[Sources|Honeypots: Tracking Hackers]]