# Level of interaction
- [[Low-interaction honeypot]]
- [[High-interaction honeypot]]
- [[Medium-interaction honeypot]]/[[Hybrid honeypot]]

<!--
|                            | [[Low-interaction honeypot]] | [[High-interaction honeypot]] |
| -------------------------- | ---------------------------- | ----------------------------- |
| Resources                  | emulated                     | real                          |
| Accuracy (& gathered info) | low                          | high                          |
| 0-day detection            | no                           | yes                           |
| Effort                     | low                          | high                          |
| [[Data control]] (& risk)  | high                         | low                           |
-->

![](../../../assets/2021-10-13-18-25-16.png) [^1]

## Source
- [[Sources|ENISA: Proactive detection of security incidents II - Honeypots]]
- [[Sources|Honeypots: Tracking Hackers]]

[^1]: [[Sources|A Survey of Honeypots and Honeynets for Internet of Things, Industrial Internet of Things, and Cyber-Physical Systems]]