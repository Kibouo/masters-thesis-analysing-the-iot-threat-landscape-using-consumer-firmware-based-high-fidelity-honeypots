# High-interaction honeypot
- real resources
    - e.g. OS & tools
        - possibly in emulated env
    - full adversary interaction

## Pros
- possible 0-day detection
    - app version limited
- high accuracy
    - attack & infection should complete
- more & richer data
    - what is important?

## Cons
- high effort config/setup
- high effort
    - deploy
    - maintain
- limited control interactions
    - system compromise & abuse possible
    - see [[data control]]
- high resource requirement

## Source
- [[Sources|ENISA: Proactive detection of security incidents II - Honeypots]]
- [[Sources|Honeypots: Tracking Hackers]]