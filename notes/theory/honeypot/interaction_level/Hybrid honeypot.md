# Hybrid honeypot
- mix [[Low-interaction honeypot]] & [[High-interaction honeypot]]

## Source
- [[Sources|ENISA: Proactive detection of security incidents II - Honeypots]]
- [[Sources|Honeypots: Tracking Hackers]]