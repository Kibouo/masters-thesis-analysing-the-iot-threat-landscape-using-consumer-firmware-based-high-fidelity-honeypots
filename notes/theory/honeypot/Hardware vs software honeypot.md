# Hardware vs software [[honeypot]]
## Overview
|             | Hardware | Software                         |
| ----------- | -------- | -------------------------------- |
| Scalable    | no       | yes                              |
| Cost        | high     | low                              |
| Accuracy    | high     | lower (implementation dependant) |
| Maintenance | high     | low                              |

## Hardware based
### Pros
- accurate
    - effective
    - good for base-lines

### Cons
- not scalable
    - expensive
        - hardware
        - internet
        - extra equipment
            - e.g. network tap, switch, ...
    - high maintenance
        - deploy `x` identical devices == `x` times work
        - low-level dependency checks
            - e.g.
                - internet connection
                - power supply/battery
                - device itself

## Software based
### Pros
- scalable
    - cheap
        - scalable
        - [[IoT]] --> low-end device
            - cheap cloud plan
    - low maintenance
        - low-level handled by cloud/infra
        - no extra equipment
            - e.g. switch
- flexible
    - e.g. support different architectures

### Cons
- harder [[detection prevention]]
    - IP-address ranges retrievable
        - disclosed by provider
            - e.g. AWS
        - or deducible
        - less attacks
            - intentional bypass
    - cloud blocks attacks
    - [[emulation]] detection [^1] [^2]


## Source
- [[Sources|Understanding Fileless Attacks on Linux-based IoT Devices with HoneyCloud]]

[^1]: [[Sources|Detecting system emulators]]
[^2]: [[Sources|QEMU emulation detection]]