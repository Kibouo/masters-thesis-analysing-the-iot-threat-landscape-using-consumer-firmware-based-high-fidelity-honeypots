# Client-side Honeypot
- simulate user
    - i.e. client application
    - e.g. browser + plugins
- active
    - researcher connects to service
- goal
    - analyse
        - service
        - served content

## Source
- [[Sources|ENISA: Proactive detection of security incidents II - Honeypots]]
- [[Sources|Honeypots: Tracking Hackers]]