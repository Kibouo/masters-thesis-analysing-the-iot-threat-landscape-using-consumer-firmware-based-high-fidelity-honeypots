# Data analysis
- look for
    - e.g.
        - new attacks
        - forensics
        - long-term trends
- collect enough!
    - indiv. honeypot logging insufficient

## Profiling attacks [^1]
- motivation?
- breadth/depth: amount of machines affected/amount a single machine was affected
- sophistication
- concealment
- attack source
    - [[Code & Projects|p0f]]
    - [[Anonymization]] likely
- vulnerability
- (attacker) tools

## Source
- [[Sources|ENISA: Proactive detection of security incidents II - Honeypots]]

[^1]: [[Sources|A Survey on Honeypot Software and Data Analysis]]