# Data collection
- == aggregation of [[data capture]] from multiple honeynets
    - i.e. multi-site deployment

## Theory (recommendations) [^1]
- naming convention
    - id deployment site & device
- secure log transmission
- anonymise data
    - of deployed devices
    - not adversary
- sync time

## Practice
- store separate from adversary
    - ensure integrity
        - prevent edit/removal
    - off-site
- distributed or central
    - project size based
- sync time
    - between nodes
    - NTP

## Source
- [[Sources|ENISA: Proactive detection of security incidents II - Honeypots]]
- [[Sources|Honeypots: Tracking Hackers]]

[^1]: [[Sources|Honeynet: Definitions, Requirements, and Standards]]