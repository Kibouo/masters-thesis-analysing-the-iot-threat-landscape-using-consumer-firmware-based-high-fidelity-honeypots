# Data capture
- == capturing data in honeynet

## Theory (recommendations) [^1]
- store off-site
- no data pollution
    - invalidates data
    - e.g. non-adversary access
- store logs for 1year
    - firewall logs
    - packet captures
    - system activity
- real-time view
    - remote too
- auto-archive
    - for extra analysis
- standardise logging
- write-up/compromise
    - standardised
- standardise time
    - GMT
- secure sensors
    - protect integrity

## Practice
- log a lot
    - external logger
    - ingress & egress
    - log "too much"
        - needed is unknown beforehand
        - complete picture needed
- actual data?
    - e.g. [^2]
        - shell usage
        - CPU usage
        - process list
        - network packets

## Techniques
- [[Shell interceptor]] [^2]
- storage: see [[data collection]]
- unknown to adversary (duh...)

## Source
- [[Sources|ENISA: Proactive detection of security incidents II - Honeypots]]
- [[Sources|Honeypots: Tracking Hackers]]

[^1]: [[Sources|Honeynet: Definitions, Requirements, and Standards]]
[^2]: [[Sources|Understanding Fileless Attacks on Linux-based IoT Devices with HoneyCloud]]