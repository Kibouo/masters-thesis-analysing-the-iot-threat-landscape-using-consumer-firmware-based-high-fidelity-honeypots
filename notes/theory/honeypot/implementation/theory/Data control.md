# Data control
- == control adversary activity

## Why?
- prevent abuse
    - complicit in illegal activity
        - e.g. proxy, spam mail, ...
- prevent access to production
    - i.e. prevent pivot

## Theory (recommendations) [^1]
- auto & manual control
- min. 2 layers
- block-on-fail
- maintain connections state
    - ingress & egress
- config at any time
    - remote too
- hard to detect by adversary
- alert on compromise
    - min. 2 ways

## Practice
- control & monitor traffic
    - ingress
        - IP block legit scanners
            - e.g. Shodan
    - egress
        - static
            - firewall
            - block all?
                - except
                    - DNS
                    - HTTP (payload fetching)
                - too restrictive?
            - fake/proxy?
        - automated
            - preferred
            - can't monitor 24/7
            - apply control fast
                - mitigate damage
            - IDS/IPS
                - doesn't catch everything
    - other
        - bandwidth restrictions
- separate subnet
    - physical or VLAN
        - phys preferred, but needed...?
    - mix IPs /w production/DMZ
- reset feature
    - don't trust `/etc/watchdog`
        - can be removed [^2]
- hard: make not noticeable
    - [[detection prevention]]

## Source
- [[Sources|ENISA: Proactive detection of security incidents II - Honeypots]]
- [[Sources|Honeypots: Tracking Hackers]]

[^1]: [[Sources|Honeynet: Definitions, Requirements, and Standards]]
[^2]: [[Sources|Understanding Fileless Attacks on Linux-based IoT Devices with HoneyCloud]]
