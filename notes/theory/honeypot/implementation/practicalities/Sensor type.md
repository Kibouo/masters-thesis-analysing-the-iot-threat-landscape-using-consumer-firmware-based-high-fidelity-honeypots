# Sensor type
## Fat sensor
1. adversary connects to a honeypot
2. honeypot logs & analyses
3. honeypot sends logs to storage

![](../../../../assets/2021-09-29-16-06-08.png)

## Thin sensor
1. adversary connects to "hub"
    - sensor on image
2. hub proxies to load balancer
    - load balancer chooses honeypot to serve
3. hub logs & analyses
4. hub sends logs to storage

![](../../../../assets/2021-09-29-16-42-27.png)

## Source
- [[Sources|ENISA: Proactive detection of security incidents II - Honeypots]]
