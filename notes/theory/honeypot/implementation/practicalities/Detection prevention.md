# Detection prevention
- == mitigate fingerprinting

## Why?
- lose value
    - halt attack prematurely
    - blacklisted by adversaries
- injection junk data

## Techniques
### Modify behaviour
- off-the-shelf easy to fingerprint
    - make unique
        - edit config
        - edit services
            - disable
            - enable
            - modify
- scatter resources
    - e.g.
        - IPs [^1]
        - ports
        - services
    - change IP if possible
        - *elastic IP*

### Blend in
- == mimic production
- same OS
- fitting name
    - banner
    - hostname
    - domain
- mask system/kernel info [^1]
    - reveals VM
    - masking
        - spoof `/proc/cpuinfo`
        - use `initramfs`
            - no disk needed
            - kernel in main memory
- other [[honeypot]] detection techniques:
    - `lsusb` [^1]
    - more: [^2]

### Behave real
- == hide diff emulated vs. real service
- emulate OS
- config VM [^1]
    - match resources to emulated device
        - e.g. CPU, memory, storage
    - [[QEMU fingerprinting]]

## Source
- [[Sources|ENISA: Proactive detection of security incidents II - Honeypots]]
- [[Sources|Honeypots: Tracking Hackers]]

[^1]: [[Sources|Understanding Fileless Attacks on Linux-based IoT Devices with HoneyCloud]]
[^2]: [[Sources|StackExchange: Easy way to determine the virtualization technology of a Linux machine?]]