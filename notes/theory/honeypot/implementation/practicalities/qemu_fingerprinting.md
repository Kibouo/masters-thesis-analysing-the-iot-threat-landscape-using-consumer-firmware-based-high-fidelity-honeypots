# QEMU fingerprinting
- environment looks real
- hypervisor detection still possible
    - real vs. simulated differ behaviour

## CPU/memory state
- state after specific instructions (CPU "bugs"/implementation)
- hypervisor-only CPU instructions/API
- QEMU [^8]
    - some techniques not applicable
        - doesn't mean it's harder...
    - "Due to QEMU being a hardware emulator, it lacks a hypervisor, thus behaves similarly to bare-metal"
- hypervisor optimisations/implementation specifics
    - e.g.
        - cache vs. translate machine code
        - CPU/"HW" timing [^2]
        - ASM `CPUID` instruction [^9]
- hard to spoof --> edit hypervisor

### Translation block execution [^7]
- working QEMU
    1. translate machine code block
        - blocks is cached
    2. execute block
        - nothing else processed!
- during processing block
    - no interrupts
    - processing = atomic
- detection?
    - race condition on purpose
        - works on bare metal
        - impossible in VM

### Translation block caching [^7]
- working QEMU
    - translated blocks are cached
- self-editing code --> huge performance loss --> measure

### Misaligned memory access with vectorised instructions [^7]
- vectorised instructions == SIMD version of CPU instructions
- CPU memory access
    - precision up to 30bits --> last 2 bits static 0 --> memory access by multiples of 4
- access unaligned
    - normal instruction --> double fetch surrounding data --> reconstruct
    - vectorised instruction --> no HW support --> error
- detect
    - add error hook
    - real CPU --> error
    - VM CPU --> no error

## Timing test
- time specific instructions
    - empirical compare
    - e.g. ping 127.0.0.1 under load [^1]
- works --> "QEMU == slow" [^8]
- hard to spoof --> edit hypervisor/kernel

## Fingerprinting
- collect info
    - e.g. CPU ID, registers, I/O
- easy to spoof

### Tools/places to check
- unavailable/clean in [[emulation]]
    - `/proc/ide/hd*/model`: not available [^3]
    - `ls -1 /dev/disk/by-id/`
    - `lscpu`
    - `lsusb`
    - `lspci`
    - `lshw`
    - `hostnamectl`
    - `dmidecode` [^4]
        - attempted to drop --> fail
        - `/sys/class/dmi/`: not available
    - `virt-what` [^5]
        - attempted to drop --> no output
    - `systemd-detect-virt` [^6]
        - not available
            - `/proc/device-tree/`
            - `/sys/hypervisor/`
            - `/proc/xen/`
            - `/proc/sysinfo`
            - `/sys/fs/cgroup/`
            - `/sys/kernel/cgroup/`
            - `/proc/vz`
            - `/proc/bc`
        - `/proc/cpuinfo`
            - `User Mode Linux`: not present
        - `/proc/1/root` == `/`
            - chroot detect
            - they may know...
- fixed
    - `dmesg` startup info
    - `/proc/sys/kernel/osrelease` via `/proc/version`
    - `/proc/scsi/scsi`
    - `/sys/block/sda/device/model`

[^1]: [[Sources|MEDA: a Machine Emulation Detection Algorithm]]
[^2]: [[Sources|mcQEMU: Time-Accurate Simulation of Multi-core platforms using QEMU]]
[^3]: https://www.dmo.ca/blog/detecting-virtualization-on-linux/
[^4]: [[Code & Projects|dmidecode]]
[^5]: [[Code & Projects|virt-what]]
[^6]: [[Code & Projects|systemd-detect-virt]]
[^7]: [[Sources|Rethinking anti-emulation techniques for large-scale software deployment]]
[^8]: [[Sources|A First Look: Using Linux Containers for Deceptive Honeypots]]
[^9]: [[Sources|Attacks on Virtual Machine Emulators]]

# Source
[[Sources|Detecting System Emulators]]
[[Sources|StackExchange: Easy way to determine the virtualization technology of a Linux machine?]]
[[Sources|Understanding Fileless Attacks on Linux-based IoT Devices with HoneyCloud]]
