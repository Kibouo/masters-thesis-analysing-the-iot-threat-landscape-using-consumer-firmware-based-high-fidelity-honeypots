# Emulation
- why?
    - honeypot context
        - [[Hardware vs software honeypot]]
        - HW
            - expensive
            - not scalable
    - others
        - dynamic analysis
            - fuzzing
            - record interactions /w "hardware"
                - \> static analysis not enough
        - development
            - HW for testing unavailable/infeasible
- [[IoT]] == embedded
    - architecture
        - e.g. ARM, MIPS
    - [[Code & Projects|QEMU]]


## Problems
- lack/working of emulated peripherals [^3]
    - lots of errors
- non-std dev
    - everything is custom
        - HW
            - layout
                - pins
                - memory
        - SW
            - libs
            - drivers
            - kernels

## Types
### Type of system [^3]
- general purpose
    - e.g. embedded linux
- special purpose
    - real-time OS
- bare-metal
    - no/custom OS, very low-level

### User- vs. system-level [^2]
- user
    - == emu *only* target program
    - pros
        - uses host
            - available? --> works
    - cons
        - static content only
        - missing stuff
            - e.g. libs, HW, ...
        - lower accuracy (possible)
            - uses host
- system
    - == emu *full* guest
        - incl kernel & drivers
    - pros
        - static & dyn content
        - higher accuracy
    - cons
        - missing stuff
            - doesn't run
            - e.g. memory, HW, ...

### Full software vs. hybrid emulation
- full software emu
    - == full stack
        - SW & HW
    - pro
        - scalable
        - automatable
    - con
        - not 100% success
    - e.g.
        - [[Sources|Towards Automated Dynamic Analysis for Linux-based Embedded Firmware]]
        - [[Sources|Jetset: Targeted Firmware Rehosting for Embedded Systems]]
- hybrid
    - ==
        - emu SW
        - proxy HW instr to real HW
    - pro
        - 100% succes
    - con
        - not scalable
        - not automatable
    - e.g.
        - [[Sources|Avatar: A Framework to Support Dynamic Security Analysis of Embedded Systems' Firmwares]]

#### Emulate hardware
1. analyse HW
    - e.g.
        - HAL functions
        - trace HW usage (FW-HW)
2. build emulation
- e.g.
    - [[Sources|Jetset: Targeted Firmware Rehosting for Embedded Systems]]
    - lots of others [^3]

## Firmware re-hosting
- reproduce hardware enough to run emulation [^1]
    - ~~correct~~
    - functional
        - analysis
            - representative results vs. original
            - e.g. always return `NULL`...?
- functional
    - /!\ alter *some* things
    - [[Sources|HALucinator: Firmware Re-hosting Through Abstraction Layer Emulation]]
        - very manual
            1. binary static analysis
            2. specify HW calls to intercept
            3. HAL is generated
    - [[Code & Projects|ARM-X Firmware Emulation Framework]]
        - medium manual
        - sturdier/working emulation
    - Firmadyne/[[FirmAE]]
        - [[Sources|Towards Automated Dynamic Analysis for Linux-based Embedded Firmware]]
        - [[Sources|FirmAE: Towards Large-Scale Emulation of IoT Firmware for Dynamic Analysis]]
        - wipes `iptables`
            - allow host access
            - for us
                - think --> not accurate
                - real
                    - user misconfig
                    - own iptables
        - use as honeypot
            - disable fuzzer
            - remove traces of emu
                - e.g. `/firmadyne` dir
            - emulate user-interesting HW
            - fix interface
                - NVRAM returns `NULL`
                    - config strings empty
                        - e.g. IP, MAC, version
            - logging? See Honware
            - make network services work
                - paper focus? --> web server
                - other too...
        - mostly automated --> our choice
    - [[Sources|Honware: A Virtual Honeypot Framework for Capturing CPE and IoT Zero Days]]
        - not open source
    - [[Sources|Automated Dynamic Firmware Analysis at Scale: A Case Study on Embedded Web Interfaces]]
        - not open source

### Networking
- [[Sources|EmuIoTNet: An Emulated IoT Network for Dynamic Analysis]]
    - Firmadyne networking framework
        - input
            - firmware
            - network topology
        - architecture
            - firmware/app DB
            - controller
                - == orchestrator
            - network configurator
                - set IPs
            - device emulator
    - Firm tweaks
        - manually specify NVRAM vals
        - static rewrite IP
            - in binaries
    - companion app emu
        - pre-config VM
        - arch
            - desktop
            - android
    - network models
        - intra-net
            - ~~internet~~
        - inter-net
            - /w internet

## Source
[^1]: [[Sources|SoK: Enabling Security Analyses of Embedded Systems via Rehosting]]
[^2]: [[Sources|FirmAE: Towards Large-Scale Emulation of IoT Firmware for Dynamic Analysis]]
[^3]: [[Sources|Challenges in Firmware Re-Hosting, Emulation, and Analysis]]

