# MQTT emulation
- hub/broker
- client scripts generate messages

## Available vulns
- subscribe all messages
- clientId == UUID
    - UUID in topic
    - kick & spoof client
- `update` topic --> hub responds with (shell) `command`
    - command injection (not actually executed)
