# Shell interceptor
- QEMU
    - syscal intercepting?

## TTY
- [[Code & Projects|Cowrie]] <-- use
- [[Code & Projects|snoopy]]

## SSH
- example from [^1]:
    1. intercept & decrypt packet
    2. determine packet type
        - data
            - plaintext
            - shell control chars
        - resize event
            - terminal window resizing
            - why?
                - recover chars
                - distinguish human vs. script
    3. emulate terminal
        - based on data
        - log & analyze result
    4. encrypt packet & pass on

## Source
[^1]: [[Sources|Understanding Fileless Attacks on Linux-based IoT Devices with HoneyCloud]]