# FirmAE
- firmware emulation/re-hosting
    - using [[QEMU]]
<!-- TODO: read paper -->
- workings

## NVRAM
- == *Non-Volatile RAM*
- device config storage
- defaults available
    - loading attempt by FirmAE
- FirmAE limitation
    - reboot == loss data
    - want out-of-box setup for honeypot
    - **reverse engineer FW & set custom defaults**