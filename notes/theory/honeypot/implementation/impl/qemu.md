# QEMU
- full system emulation
    - translate instructions between architectures
    - in SW
    - emulate CPU & peripherals
- user mode emulation
    - namespaced ("container")
    - single applications
        - not full OS
