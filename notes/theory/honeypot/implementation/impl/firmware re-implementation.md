# Firmware re-implementation
- smart home devices
    - not available

## Scratch pad
https://ec.europa.eu/competition/antitrust/sector_inquiries_internet_of_things.html

### APIs
#### Google Home
https://rithvikvibhu.github.io/GHLocalApi/#section/Google-Home-Local-API

#### TP-link
https://github.com/plasticrake/tplink-smarthome-simulator
https://www.softscheck.com/en/reverse-engineering-tp-link-hs110/

#### Philips Hue
https://austenclement.com/getting-started-with-the-philips-hue-rest-api/
https://github.com/Mengmengada/ThingPot
https://github.com/studioimaginaire/phue
https://www.xmpp-iot.org/tutorials/python-philiphshue/

#### Bosch Smart Home
https://bosch-iot-suite.com/iot-devices/smart-home-controller/
https://apidocs.bosch-smarthome.com/local/
https://github.com/BoschSmartHome/bosch-shc-api-docs
https://github.com/tschamm/boschshcpy

#### Other
https://insteon.docs.apiary.io/#introduction/limitations
https://github.com/itead/Sonoff_Devices_DIY_Tools/blob/master/SONOFF%20DIY%20MODE%20Protocol%20Doc%20v1.4.md
https://github.com/arm5077/mirai-honeypot
