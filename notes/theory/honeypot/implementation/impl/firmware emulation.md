# Firmware emulation
- [[FirmAE]] used
    - Firmadyne improved
    - not stealthy

## Problems
- FW fail to extract
    - e.g.
        - UBIFS (new ASUS)
        - bins /w custom extraction (old Dlink have `.bin`, new Trendnet have `.dav`, Bosch IPcam `.fw`)
        - changes-only files
    - mainly new-ish IP cams
        - **had to fall back to old, known to emulate**
- services don't start
    - sometimes even if [[FirmAE]] could ping them...
    - **only [[FirmAE]] problem**
    - e.g. for new routers: web server

## FW Availability
- old patches unavailable
- "classic" devices
    - 99%
    - e.g.
        - IP cam
        - router
        - AP
        - switch
    - NOTE
        - popular companies
            - FW available
            - many CVEs
        - lesser known companies
            - FW hard to find/unavailable
            - low amount/no CVEs
- smart home
    - found none
        - **FirmAE cannot test**
            - original
                - only router & IP cam
                - we can't contribute
    - update via app
        - **possible improvement**
            - limits research
                - vendor should release!
            - other thesis
                - break app?
    - e.g.
        - lights
        - hub
        - thermostat

## Design honeypot network
- edge
    - == external visible
    - [[FirmAE]] emulation
        - prove adversary can interact
        - high fidelity
    - e.g. router & IP cam
- inner
    - == internal visible only
    - self-made
        - adversary already proven themselves
        - replicate developer API
        - low fidelity
    - e.g. hub & smart devices
