# Telnet Proxy
- popular
    - telnet /w default creds [^1]

## low-interaction proxy
- example
    - [[Low-interaction honeypot]] proxy'd to DVIC
        - *Damn Vulnerable IP Camera* 😄
        - helps fragile emulation from being knocked over
        - allow for [[Honeytoken]]
        - allow [[shell interceptor]]

## Source
[^1]: [[Sources|Beyond Telnet: Prevalence of IoT Protocols in Telescope and Honeypot Measurements]]****