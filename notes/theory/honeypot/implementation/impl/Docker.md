# Docker
- IoT network simulation
- separate
    - host: captor
    - containers: decoy

## Exposing ports
- expose on host's `0.0.0.0`
    - simply map ports

## Health monitoring
- auto restart containers
    - when broken
        - DoS
        - other stuff

## Hardening [^1] [^2]
- goal
    - prevent escape --> abuse
    - no stealth
        - escape honeypot --> already knows
- how?
    - resource limitation
    - downgrade default user
    - apparmor
    - user namespaces
        - not possible due to `--privileged` requirements
    - remove container capabilities
        - not possible due to `--privileged` requirements

### Not applicable
- [[FirmAE]]
    - uses `--privileged`
        - lots of capabilities enabled
        - easy container escape
    - hardening mostly unneeded
        - container access requires QEMU escape
    - done: resource limitation
- [[MQTT emulation]]
    - done
        - resource limitation
        - downgrade default user
            - uid 1000 --> log ownership

## Source
[^1]: [[Sources|How To Harden Your Docker Containers]]
[^2]: [[Sources|Docker Breakout / Privilege Escalation]]