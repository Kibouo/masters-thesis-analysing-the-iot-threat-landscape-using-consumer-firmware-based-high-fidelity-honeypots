# Believability
- [[detection prevention]] to prevent basic [[QEMU fingerprinting]]
- things we did to make it more believable
    1. bandwidth & processing limitation
    2. use 192.168.0.1/24 for all devices
    3. same QEMU IP as Docker container
    4. populate NVRAM
    5. "live" camera feed
    6. chroot/hide firmadyne artifacts (file & process & ENV vars)
        1. hide processes
           - used: [`hidepid`](https://linux-audit.com/linux-system-hardening-adding-hidepid-to-proc/)
                - pros
                    - great for non-root user
                    - hides in `ps` and `/proc` dir
                - cons
                    - hides kernel processes, e.g. `[kthreadd]`
                        - knowledgable person might realise something is going on
                    - root can still see everything
                        - adversaries will want root access
            - alternative: [libprocesshider](https://github.com/Kibouo/libprocesshider)
                - pros
                    - hides even for root user
                    - hides in `ps`
                - cons
                    - doesn't hide/prevent access to `/proc` dir
        2. non-root user setup
        3. disable `dmesg` for non-root
        4. spoof system info
            - prevent [[QEMU fingerprinting]]
            - solution
                - mount over it
                    - `mount` shows individual file mounts
                        - fixable?
                            - namespaces (fails on FW)
                - `hppfs` (unavailable)
                - hide/exclude files and add "normal" files instead (fail)
                    - hide (requires FUSE)
                        - e.g. https://www.linux-magazine.com/Online/Features/Cmdfs
                    - exclude
                        - not possible to `mount` dir and exclude files
                - kernel module to mess with procfs (kernel modules fail to load)
    7. NOT DONE: namespaces
        - would allow hiding mounts, PIDs, make user id 0 inside and other outside, etc.
        - problem: `unshare` doesn't work
    8. proper MAC addresses for QEMU- & Docker interfaces
        - 1st half MAC is vendor prefix
        - seen by ARP scan
    9. spoof hostnames
        - unneeded --> hostnames are via DNS
            - QEMU devices don't know about Docker's DNS
            - there should be no access to Docker containers

## Perfect feasible?
- no
    - virtual vs. real [^1]
    - fix all virtual? --> "usage" artifacts [^2]
- "not a problem, VMs will be used a lot" [^1]
    - they are, but not for IoT
    - QEMU speed ≈≈ IoT speed
- **hosting provider (OVH & AWS) IP ranges are public**
    - shows on e.g. (partial) shodan, etc. scan

[^1]: [[Sources|Spotless Sandboxes: Evading Malware Analysis Systems using Wear-and-Tear Artifacts]]
[^2]: [[Sources|Compatibility is Not Transparency: VMM Detection Myths and Realities]]