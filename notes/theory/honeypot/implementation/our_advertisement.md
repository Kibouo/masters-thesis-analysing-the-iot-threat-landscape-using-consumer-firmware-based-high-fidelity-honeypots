# Our advertisement
- [[advertisement]] via [[honeytoken]]
- multiple rounds for visibility
    - needed [^2][^3]

## Possible places to distribute
- google document [^1]
- text sharing (usually crawled --> consistent structure[^6]) [^2]
    - PasteBin [^5]
    - HasteBin
    - pastelink.net
    - ideaone.com
    - justpaste.it
    - pastie.org
    - pasted.co
    - Github Gist
    - VirusTotal
    - controlc.com
- Hacking forums [^2]
    - alive
        - nulled.to
        - hackforums.net
    - dead
        - offensivecommunity.net
        - bestblackhatforums.eu
- dark web [^2]
    - sell leaked creds (give sample --> honeytokens)
- keyloggers [^4]

## Tokens (passwords)
### Template
```json
// exposure_report.json
{
  "info": "exposure report",
  "time": 1644160351,
  "device": [
    {
      "ip": "<ip_here>",
      "port": 23,
      "username": "admin",
      "password": "<pass_here>",
      "service": "telnet"
    },
    {
      "ip": "<ip_here>",
      "port": 80,
      "username": "admin",
      "password": "admin",
      "service": "httpd",
      "hostname": "DCS-700L",
      "device-type": "ipcam",
      "path": "/home.htm"
    }
  ]
}
```

### Round 1 (28/01/2022)
#### VPS
- UXhxqkUP9C2F
    - https://hackforums.net/showthread.php?tid=6190896
    - ![](../../../assets/2022-01-27-16-43-31.png)
    - alert: ![](../../../assets/2022-02-06-15-18-01.png)
- ueYQSt8gHB15 (controlc.com)
    - https://controlc.com/8656df6c
    - ![](../../../assets/2022-01-27-16-51-44.png)
- IotHpxtHrRO9
    - https://pastebin.com/W9eiXc2j
    - ![](../../../assets/2022-01-27-16-53-27.png)
- xI5cJJTDvHZp
    - https://www.toptal.com/developers/hastebin/adobenosid
    - ![](../../../assets/2022-01-27-16-56-09.png)
- ~~XYhCnIMGxXTL~~
    - github gist account got flagged
    - github & gmail account
        - user: github.anon.iot@gmail.com
        - pass: +t=`M{L<O1P?{;Kp
    - https://gist.github.com/anon-i-o-t/15fce062f0f2a4e9e3a140d6d1f24f88
    - ![](../../../assets/2022-01-27-17-11-32.png)

#### EC2
- eFqvJKFkUHGg (raidforums.com)
    - https://raidforums.com/Thread-Hacking-webcam
    - ![](../../../assets/2022-01-27-16-44-15.png)
- Vjuz1DKINrAl
    - https://controlc.com/8656df6c
    - ![](../../../assets/2022-01-27-16-51-44.png)
- U2qZGYhlxmXl
    - https://pastebin.com/W9eiXc2j
    - ![](../../../assets/2022-01-27-16-53-27.png)
- de3Z8reuqm8y
    - https://www.toptal.com/developers/hastebin/ayezikidej
    - ![](../../../assets/2022-01-27-16-56-52.png)
- ~~EqInNZgSayiS~~
    - github gist account got flagged
    - github & gmail account
        - anonym.iot@gmail.com
        - qO"$~8&tz1;x"uMR
    - https://gist.github.com/anonym-iot/3c8f7e47200674bd7180d70cbbe9f850
    - ![](../../../assets/2022-01-27-17-21-38.png)

### Round 2 (06/02/2022)
#### VPS
- leTc8t
    - https://controlc.com/8b7dbe8f
    - ![](../../../assets/2022-02-06-16-18-59.png)
- Jz8MfY
    - http://codepad.org/ZJBgasrh
    - ![](../../../assets/2022-02-06-16-18-44.png)
- cd3XwE
    - http://pastie.org/p/0TpiYjW1xRd3BrvZerylxD
    - ![](../../../assets/2022-02-06-16-18-32.png)
- GKfayV
    - https://pastebin.com/GaMMFpu0
    - ![](../../../assets/2022-02-06-16-18-21.png)
- CmA45I
    - https://hastebin.skyra.pw/oxuwihewik.json
    - ![](../../../assets/2022-02-06-16-18-10.png)

#### EC2
- 6j6AaR
    - https://controlc.com/66e8b3de
    - ![](../../../assets/2022-02-06-16-21-44.png)
- HdYmdc
    - http://codepad.org/oGWCuCB9
    - ![](../../../assets/2022-02-06-16-22-02.png)
- dYm78N
    - http://pastie.org/p/2sEkB4ezkPIucnqL8KypNY
    - ![](../../../assets/2022-02-06-16-22-10.png)
- jXO0DZ
    - https://pastebin.com/DnjJ6RMc
    - ![](../../../assets/2022-02-06-16-22-28.png)
- xNcr9A
    - https://hastebin.skyra.pw/kalehakume.json
    - ![](../../../assets/2022-02-06-16-22-45.png)

### Round 3+ (15/02/2022)
- wrote bot to post every 6 hours on pastebin-like websites
- format
    - EC2 --> URI-like
    - VPS --> JSON

### Round 4 (21/02/2022)
- wrote google docs, made public, & posted on pastebin

#### EC2
- https://docs.google.com/document/d/e/2PACX-1vTj-M2LC_l6ppls_y42pa05dx8kDr4aMy5-TkuLKLwqdZPa_q6Lb2R6UI1R4TlZtI--39siEl7wtS7M/pub
- ![](assets/2022-02-21-16-42-21.png)
- pastebins
    - https://pastebin.com/zPSE5EwS
    - https://pastebin.com/5diBQ6t1

#### VPS
- https://docs.google.com/document/d/e/2PACX-1vRFdce_iQq320yTJoZi-HeFKrYHj_ZXEYn1j_cf65chvO2K7oH0E2AH9Oc5Em5M7ycT3RnSCqj7vgLv/pub
- ![](assets/2022-02-21-16-42-54.png)
- pastebins
    - https://pastebin.com/Bv2n5xGA
    - https://pastebin.com/TbXTUeRJ

### Round 5 (06/03/2022)
- replace VPS JSON template with RFC URI template
    - https://datatracker.ietf.org/doc/rfc4248/

### Round 6 (10/03/2022)
- VPS nulled.to forum post
    - https://www.nulled.to/topic/1383064-public-webcam/

    - ![](../../../assets/2022-03-10-15-30-13.png)

### Round 7 (22/03/2022)
- VPS: disable access to automated intrusion

### Round 8 (05/04/2022)
- VPS: disable proxy, giving direct access to webcam's Telnet service

### Round 9 (14/04/2022)
- VPS: convert to challenge
    - Telnet creds: `admin`/`xHLJs4IZ`
    - flag published by smart lock over MQTT: `FLAG{fun_within_constraints}`
    - publish with questionnaire

## Sources
[^1]: [[Sources|Honey sheets: What happens to leaked google spreadsheets?]]
[^2]: [[Sources|Who Is Reusing Stolen Passwords? An Empirical Study on Stolen Passwords and Countermeasures]]
[^3]: [[Sources|Defining Who Is Attacking by How They Are Hacking]]
[^4]: [[Sources|Harvesting Wild Honey from Webmail Beehives]]
[^5]: [[Sources|TrendMicro: Caught in the Act: Running a Realistic Factory Honeypot to Capture Real Threats]]
[^6]: [[Sources|Picky Attackers: Quantifying the Role of System Properties on Intruder Behavior]]