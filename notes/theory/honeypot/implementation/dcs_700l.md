# DCS-700L
- d-link ipcam

## Problems
- NVRAM
    - `libnvram.so` doesn't match FW's function definitions
    - `/dev/gpio` missing --> some NVRAM not loaded
        - how do we know?
            - ghidra: `gp=>_mips_gp0_value`
            - `strace alphapd`
                - `open("/dev/gpio", O_RDONLY) = -1 ENXIO (No such device or address)`
        - couldn't fix it
            - kernel module loading fails in QEMU
            - create random file at `/dev/gpio`
                - error
                    - "not a TTY"
                    - need specific behaviour
        - hardcode in HTML
            - mainly system stuff
                - e.g. IP, MAC
            - configurable stuff still via NVRAM --> adjustable
                - e.g. camera lighting, admin passw, etc.
- camera feed
    - loading `image.jpg` triggers camera capture
    - obviously not possible
    - tweak HTML & add script to rotate between pre-defined images

## Login
- default creds `admin`/``
    - combine /w telnet proxy
        - chroot
        - hide PIDs
        - change pass