# Implementation
## Define goals [all] [^1]
- e.g.
    - purpose
    - cost
    - services
    - liability

## Theory
- [[Data control]]
- [[Data capture]]
- [[Data collection]]
- [[Data analysis]]

## Practicalities ([[Server-side honeypot]] focused)
- [[Advertisement]]
- [[Placement]]
- [[Level of interaction]]
    - combination possible
        - == proxy [[Low-interaction honeypot]] to [[High-interaction honeypot]]
- [[Sensor type]]
- [[Detection prevention]]

## Our implementation
- [[Docker]]
- [[Storage]]
- [[Resource limitations]]
- [[Believability]]
- [[high-interaction honeypot]]
    - [[Firmware emulation]]
        - [[QEMU]]
        - [[DCS-700L]]
- [[low-interaction honeypot]]
    - [[MQTT emulation]]
- host services
    - [[Firewall]]
    - [[Health check]]
    - [[Network tap]]
    - [[Telnet proxy]]
        - [[Shell interceptor]]
- [[Our advertisement]]

## Source
- [[Sources|ENISA: Proactive detection of security incidents II - Honeypots]]
- [[Sources|Honeypots: Tracking Hackers]]

[^1]: [[Sources|A Survey of Honeypots and Honeynets for Internet of Things, Industrial Internet of Things, and Cyber-Physical Systems]]
    - section IX A.