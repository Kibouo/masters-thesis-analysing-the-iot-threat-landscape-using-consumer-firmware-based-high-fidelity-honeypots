# Honeytoken
- data resource
    - e.g. email, DB record
- goal
    - analyse
        - associated activity
    - identify/track
        - breach
        - inside-threat

## Requirement
- [[Honeypot]] requirements
    - no system interference: no authentic data pollution
- important looking
    - piques adversary interest
- unique tokens
    - no false positives (mix tokens)

## Source
- [[Sources|ENISA: Proactive detection of security incidents II - Honeypots]]
- [[Sources|Honeypots: Tracking Hackers]]