# Anonymization
- attack source IP
    - != attacker (likely)
    - == exit node anonymization network
        - e.g.
            - Tor exit node
            - VPN server
            - proxy server
                - commercial
                - compromised system
    - IPv4 <-> host changes [^1]
        - due to pooling
        - constant within ~1 day

## Source
- [[Sources|Distributed and highly-scalable WAN network attack sensing and sophisticated analysing framework based on Honeypot technology]]

[^1]: [[Sources|A Survey on Honeypot Software and Data Analysis]]