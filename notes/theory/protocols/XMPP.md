# XMPP
- == Extensible Messaging and Presence Protocol

## What?
- structured messaging
    - XML application profile
    - small packets
    - address msg at other client
- features
    - routing
    - extensible
- architecture
    - logically: P2P
        - client addresses client
    - actually: distr network client & server
        - client-server
            - network edge routing
        - server-server
            - inter-domain routing
                - dst registered at other server?
- networking
    - TCP
- security
    - TLS
        - optional
    - SASL
        - forced

### Extensible
- features
    - instant messaging [^1]
    - discovery [^1]
    - reliable delivery [^5]
- architecture
    - M2M [^2]
    - publish-subscribe [^3]
- networking
    - HTTP [^4]

## How?
### General
#### Client-Server
1. get endpoint (ip:port) & connect
    - DNS
2. setup TCP
3. setup XML stream
4. TLS nego
    - recommended
    - not required
5. auth
    - required
6. bind resource to XML stream
7. send data
8. close XML stream & TCP

#### Server-Server
1. get endpoint (ip:port) & connect
    - DNS
2. setup TCP
3. setup XML stream
4. TLS nego
    - recommended
    - not required
5. auth
    - required
6. send data
    - server-server stuff
    - client data to be routed
7. close XML stream & TCP

### Messaging
#### XML stream
- == logical connection
- communication protocol == 1 big XML 🙃
    - e.g. can start as any other XML doc: indicate XML version & text enc
    - indicate
        - open: `<stream>`
        - close: `</stream>`
    - packets
        - handshake (encryption/authentication)
            - XML elements
        - *XML stanza*
            - == data
            - i.e. message, presence, IQ (Info/Query)
            - XML elements
        - stream ctrl
            - e.g features, errors
- unidirectional
    - data
    - ctrl is 2 dir
    - recv answer?
        - negotiate response stream
            - i.e. send `<stream>` as over same TCP

#### Resource binding
- resource
    - == service
- bind data to service (+ user)
    - not server/user only
- multiple?
    - possible
    - 1/stream
        - ~~1/TCP~~

#### Routing
- DNS based
- email-like
    - share address book
    - addresses
        - server: `example.com`
        - client: `user@example.com`
        - service `user@example.com/service`
- flow
    - src & dst client connected to
        - same server
            - `src_client --> server --> dst_client`
        - diff server
            - `src_client --> local_server --> remote_server --> dst_client`

### Security
- stack
    1. TCP
    2. TLS
    3. SASL
    4. XMPP
- min. technical requirements if using
    - authentication
    - confidentiality
- require
    - b64 parsing
        - fail on error
    - ~~SSLv2.0~~
        - not secure
- warning
    - DNSSEC
    - good hashes
    - SASL
        - server lists mechanisms
        - downgrade?
            - use TLS!
    - UTF-8
        - char spoofing
    - XML issues
        - XMPP prevents e.g. external ref
    - DoS
        - XMPP only process auth data
        - tip: limit traffic
            - e.g. firewall, data
- prevent leaks
    - by server
        - of other clients
            - IP & access
            - presence
            - user/resource
                - dir harvest
                - return: generic error
                - timing attacks?

## Source
- [[Sources|RFC 6120: Extensible Messaging and Presence Protocol (XMPP): Core]]

[^1]: [[Sources|RFC 6121: Extensible Messaging and Presence Protocol (XMPP): Instant Messaging and Presence]]
[^2]: [[Sources|XEP-0166: Jingle]]
[^3]: [[Sources|XEP-0060: Publish-Subscribe]]
[^4]: [[Sources|XEP-0124: Bidirectional-streams Over Synchronous HTTP (BOSH)]]
[^5]: [[Sources|XEP-0198: Stream Management]]