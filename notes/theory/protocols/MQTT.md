# MQTT
- ~~== *Message Queuing Telemetry Transport*~~
    - not really 🙃
        - no queue

## What?
- client-server architecture
    - publish-subscribe messaging
- QoS support
- networking
    - TCP
    - IP
    - *high power usage?*

## Where?
- constrained env
    - low computing power
        - RAM
        - ROM
    - low power networking
        - bandwidth

## Specifications
### QoS
0. <= 1
1. 1+
2. == 1

### Encryption
- **not provided**
- recommend
    - TLS
    - VPN
- *TLS is heavy!*

### Authentication
#### Of client by server
- **impl dependant**
    - **possibly no support**
    - CONNECT packet
        - `username` & `password` fields
            - `password` field can contain token
        - v5.0
            - enhanced auth: challenge/response support
    - SSL cert
    - external mechanism, e.g.
        - LDAP
        - OAuth
        - OS-based
    - own mechanism
        - custom impl

#### Of server by client
- **not provided**
- recommend
    - SSL cert
    - v5.0
        - leverage enhanced authentication
        - impl dependant
            - client & server support needed

### Authorization
- **not provided**
- recommend
    - based on e.g.
        - authentication result
        - IP
        - `ClientId`

#### Pre-v5.0
- **impl dependant**
    - **possibly no support**

#### v5.0
- *should*
    - *but*
        - non-normative
            - i.e. not official 🙃
        - not explained *how*
    - check client
        - before accepting connection
        - allowed to use `ClientId`
    - access control
        - publish
        - subscribe
        - limit wildcard subs

### Other
- impl dependant
    - SSL cert
        - revocation list (CRL) check
    - behaviour monitoring
        - dynamic block
        - based on e.g.
            - connection attempts
            - authentication attempts
            - topic scanning

## Vulnerabilities
- DoS & RCE [^2]
    - buggy implementation
        - packet parsing
        - UTF-8 handling

### Encryption
- MITM

### Authentication
- rogue clients
- `ClientId` abuse [^1]
    - attack?
        - no `ClientId` auth
            - CONNECT auths account!
            - i.e. allowed to use `ClientId`? Not checked
        - new client /w already connected `ClientId` --> kick old client
        - vulns
            - DoS
            - session hi-jacking
                - session tied to `ClientId`
                - `Clean Session: 0` flag in CONNECT
    - acquire?
        - (educated) guess
            - id is
                - unique
                    - spec requirement
                - usually not secret
                    - semantics known? --> enumerate!
        - previous access
        - from topic paths
            - usually critical device info encoded
                - e.g.
                    - `/<deviceType>/<MAC>`
                    - `ClientId` is maybe based on MAC

### Authorization [^1]
- over-privileged access
    - improper control
- topic sniffing
    - acquire path?
        - wildcards
            - buggy access control checks
        - (educated) guess
            - semantics
                - device properties
                - device capabilities
                - enumerable number
                    - e.g. device id
        - previous access
- rogue paths
    - use as C&C
    - e.g.
        - victim subs to `/<deviceType>/<id>/+`
        - adversary publishes to `/<deviceType>/<id>/attack`

### Access revoking [^1]
- i.e. clean-up
- insecure *shared* usage of (embedded) device
- spec --> no info

#### Affect future
- what?
    - will message
    - retained message
- how?
    - retained after privilege loss

#### Eavesdrop
- why?
    - bad session management
        - impl dependant
    - remove client authorization
        - not in spec
- what?
    - removed authorization
        - no new actions
            - e.g. subscribe, publish
        - **subs kept!**

#### Rogue device
- user device vs. embedded device
    - distinction
        - [[IoT]]/framework
        - not [[MQTT]]
- revoking authorization
    - user device
        - "easy"
        - multiple controllers can exist
    - embedded device
        - removing permissions --> bricked device
- get embedded device creds
    - abuse any time
    - cred cycling
        - just keep session open 🙃

## Mitigations
- `ClientId`
    - couple to account authentication
    - sessions? --> keep id secret
- sessions
    - retroactively apply account changes
        - e.g. privilege adjustment
- access control
    - per message
    - based on properties
        - of
            - sender
            - receiver
            - message
    - check perms at time of usage

## Source
- [[Sources|OASIS Standard: MQTT Version 5.0]]

[^1]: [[Sources|Burglars' IoT Paradise: Understanding and Mitigating Security Risks of General Messaging Protocols on IoT Clouds]]
[^2]: [[Sources|TrendMicro: The Fragility of Industrial IoT's Data Backbone]]