# CoAP
- == *Contrained Application Protocol*

## What?
- HTTP-like
    - REST-like
        - allow small update
        - e.g. GET, POST
    - URIs
    - easy interface /w web
- M2M architecture
    - request-response messaging
- discover services & resources
- encryption
    - DTLS
        - resource heavy
            - optimisations being researched [^3]
    - IPSec
- networking
    - UDP
    - 6LoWPAN
        - == *IPv6 over Low-Power Wireless Personal Area Networks*
    - congestion control
    - ports
        - `5683`
            - default
            - resource discovery
                - required
        - `5684`
            - DTLS
            - resource discovery
                - may support

## Where?
- constrained env
    - low computing power
        - RAM
        - ROM
    - low power networking
        - lossy network
        - bandwidth

## Features
1. UDP
    - reliability
        - optional
    - unicast
    - multicast
        - UDP only
        - not in DTLS
2. DTLS
    - TLS over UDP
3. async
4. web protocol
    - M2M
    - constrained env
5. low overhead
    - header
    - parsing
6. HTTP technology
    - URI
    - Content-Type
7. proxy & caching
8. stateless HTTP mapping
    - HTTP proxy & caching support

## How?
- endpoint
    - client & server
    - != HTTP

### Messaging
- multicast support
- 2 "layers"
    - mental model! 1 protocol
    1. UDP layer (network)
    2. application layer

#### Congestion control
- ensure delivery
    - CON packet
    - expect resp
    - no resp? retransmit
        - exponential back-off
    - limit outstanding interaction
        - i.e. unfinished interaction
        - expect resp
- improvement needed [^4]

#### `Message ID` vs `token`
- `Message ID`
    - "network layer"
    - match messages between req/resp
    - reliability
        - TCP stream "emulation"
- `token`
    - "application layer"
    - match data between req/resp
    - application-level communication
        - "here is data for request *x*"

#### Message types ("network layer")
1. CON
    - ensure delivery
        - timeout
        - ACK resp
            - `Message ID` to identify
        - retransmit
2. NON
    - no ACK needed
        - i.e. dump data
    - e.g. 1 data point from continuous monitoring by sensor
3. ACK
    - confirm CON
4. RST
    - failed to process
    - *not* "400 not found"/"500 no permission"
    - multicast? --> don't send
        - error overload

#### Request-Response ("application layer")
- messaging
    - piggybacking
        - ACK /w data
        - ```
            Client      Server
            |    CON    |
            |    -->    |
            |           |
            |    ACK    |
            |     +     |
            |   data    |
            |    <--    |
            ```
    - separate responses
        - after delay
            - same `token`
            - different `Messaging ID`
        - CON/ACK
            - ```
              Client      Server
                |    CON    |
                |    -->    |
                |           |
                |    ACK    |
                |    <--    |
                |           |
               ... [delay] ...
                |           |
                |    CON    |
                |     +     |
                |   data    |
                |    <--    |
                |           |
                |    ACK    |
                |    -->    |
                ```
    - no confirmation
        - ```
          Client      Server
            |    NON    |
            |    -->    |
            |           |
          (... [delay] ...) (maybe)
            |           |
            |    NON    |
            |     +     |
            |   data    |
            |    <--    |
            ```
- request
    - method code
    - payload
        - URI
        - data
        - method
            - e.g. GET, POST
- response
    - response code
    - maybe payload
- `token`
    - match application-level request-response data

### Discovery [^1]
- unicast or multicast
- discover services
1. GET `/.well-known/core`
    - return **CoRE Link Format** data
2. client filters
    - for
        - resource type
        - interface description
        - media type

## Vulnerabilities [^2]
- parsing
    - impl bug?
- cache/proxy
    - == MITM
    - pass on?
        - use same/stricter
            - encryption
            - access control
        - impl bug?
- IP spoof
    - UDP --> no stream
    - attacks
        - + amplification --> [[reflection attack]]
            - prevent: factor dependant on authentication
        - disrupt logic of message flow
            - e.g.
                - spam RST for DoS
                - spoof response
        - data exfiltration
            - bypass firewall
        - cross-protocol attack
            - send
            - recv

## Source
- [[Sources|Constrained Application Protocol for Internet of Things]]
- [[Sources|RFC 7252: The Constrained Application Protocol (CoAP)]]

[^1]: [[Sources|RFC 6690: Constrained RESTful Environments (CoRE) Link Format]]
    - section 1.2.1
[^2]: [[Sources|RFC 7252: The Constrained Application Protocol (CoAP)]]
    - section 11
[^3]: [[Sources|Security as a CoAP resource: An optimized DTLS implementation for the IoT]]
[^4]: [[Sources|CoAP Congestion Control for the Internet of Things]]