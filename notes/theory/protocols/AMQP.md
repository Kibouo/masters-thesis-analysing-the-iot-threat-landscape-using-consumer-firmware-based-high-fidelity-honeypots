# AMQP
- == *Advanced Message Queueing Protocol*

## What?
- framing & transfer protocol
- support
    - M2M
    - binary data encoding
        - application interop
            - even /wo 100% schema same
    - reliable delivery [^1][ch.2.6.12]
        - [[MQTT]]'s QoS
        0. <= 1
        1. 1+
        2. == 1
    - frame & message handling
- 2 layers
    - transport
        - binary
        - reliable delivery
    - messaging
        - abstract format
        - standard encoding
- networking
    - reliable --> TCP
- security
    - TLS/IPSec
    - SASL

### Extensible
- topology **not** defined
- common impl
    - queueing
    - routing
    - architecture
        - e.g.
            - client-server
                - publish-subscribe
            - M2M
                - request-response

## Where?
- business messaging
    - require
        - reliability
        - speed

## How?
### Type encoding
#### Types
- primitive
    - scalar
    - collection
- described
    - == alias for apps which understand
    - no understand? fallback primitive
- composite
    - == struct
    - description required
- restricted
    - closed
        - ≈≈ enum
    - open
        - subset primitive
            - e.g. URL == string /w semantics

#### Encoding
- `[constructor][untyped bytes]`
- `[constructor]`
    - bytes describing interpretation `[untyped bytes]`
    - BNF grammar
        - recursive
            - composite support

### Transport
- binary
- M2M
- link vs channel?
    - diff level of conceptualism
        - different properties/focus
    - link can be restored into diff channel/session/connection

#### Conceptual (link-node)
- linked nodes network
    - nodes ... messages
        - send
        - recv
        - proxy
- node
    - == service
        - e.g. producer, consumer, queue/proxy
    - contains messages
        - responsible
            - safe storage
            - delivery
        - responsibility follows message
- link
    - == node 2 node connection
    - 1 direction (`->`)
        - data
        - ctrl = 2 dir
    - attach at *terminus*
        - ≈≈ socket /w function
        - source vs target
    - state
        - ensure delivery
    - named
        - allow recovery

#### Actual (container-channel)
- connection
    - == "full-duplex, reliably ordered sequence of frames" [^1]
    - multiplexing
        - using channels
    - contains sessions
        - == 1+ channels
            - logical group
        - state
            - credit-based flow control
                - count `transfer` frames
                    - data frames
                - load balancing
- container
    - == application
    - == 1+ nodes
- channel
    - == node 2 node connection
    - 1 direction (`->`)
        - data
        - ctrl = 2 dir
    - tagged
        - allow multiplexing

### Messages
#### Types
- bare
    - == body
    - parts
        1. std prop
        2. app prop
        3. app data
    - immutable
- annotated
    - == bare + annotations
    - annotation
        - == header & footer
        - types
            1. permanent
            2. consumed by next node

#### Terminus
- source
    - filter outgoing
        - *pure* function
        - every node checks b4 sending msg out
        - e.g. auth msg at every node, access control
    - define distribution-mode
        - move
            - edit msg state
        - copy
            - const msg state
            - sent? use other marker
    - success transfer
        - NEVER re-send
        - prevent 2+ msg
- target

#### State
- distribution node
    - contains msg
    - most nodes?
        - not control node
- per msg, per distribution node
- based on
    - transfer state
    - delivery state
- ![](../../assets/2021-10-07-16-39-31.png) [^1]
    - start @ `AVAILABLE`
        - trait: transferable
            - can `TRANSFER (acquiring)`
                - edit state
                    - `AVAILABLE` ==> `ACQUIRED`
                    - b4 send!
    - `ACQUIRED`
        - not transferable
            - prevent 2x msg

### Transactions
- coordinated/grouped indep transfers
    - how?
        - tag transfers
            - `txn-id`
    - any amount
    - any direction
- container types
    - controller
        - init & conclude transaction
        - setup *control* link
            - control frames only
            - to coordinator
    - resource
        - perform transaction work
- coordinator node
    - specified by resource
    - not exclusive to transaction

### Security
- in handshake
- TLS
- SASL
    - authentication
    - authorization

## Source
- [[Sources|OASIS Standard: Advanced Message Queueing Protocol (AMQP) Version 1.0]]
- [[Sources|Microsoft: AMQP 1.0 in Azure Service Bus and Event Hubs protocol guide - Azure Service Bus]]

[^1]: [[Sources|OASIS Standard: Advanced Message Queueing Protocol (AMQP) Version 1.0]]