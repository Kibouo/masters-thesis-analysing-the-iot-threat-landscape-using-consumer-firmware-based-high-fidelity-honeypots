# UPnP
- *Universal Plug and Play*

## What?
- discover & control services
    - device == 1+ services
- 0-config
- M2M architecture
    - request-response messaging
    - pub-sub (eventing)
- Control Point (CP) == UPnP enabled device
- networking
    - IP
        - addressing
    - UPD
        - discovery
        - eventing
    - TCP
        - content transmission
    - XML /w SOAP
        - content
- cloud env --> run over [[XMPP]]

## Where?
- ad-hoc/unmanaged networks
    - e.g. home, small office

## How?
0. networking
    1. user connects device (eth/wifi pass)
    2. get IP (DHCP/Auto IP)
1. discovery
    - [[SSDP]] uses
        - port: `1900`
        - bcast address: `239.255.255.250`
    - mode
        - active
            1. bcast search
                - possible: specify service type
                - using [[SSDP]] M-SEARCH
            2. wait for answer
                - unicast response
        - passive
            1. listen for [[SSDP]] NOTIFY
                - on bcast address
    - contains: basic info
        - URL /w advanced info
        - type, id, ...
2. description
    - fetch advanced info from URL
        - contains
            - URL for control, eventing, presentation
            - list services
            - per service
                - actions & arguments list (control)
                - variables == state (eventing)
            - model name/nr., manufacturer name/URL, ...
3. control
    - send instructions
        - == change state
        - destination: control URL
        - payload: action + arguments
4. eventing
    - variable (state) change --> send update (event)
        - payload: variable + value
    - subscribe to event
        - init event == all vars --> baseline init
    - bcast events
5. presentation
    - web page URL
    - control and/or view state

## Vulnerabilities [^1]
- no
    - authentication
    - authorization
- UPnP IGD (Internet Gateway Device) control
    - i.e. UPnP enabled modem
    - can config
        - portmapping (firewall)
            - expose [[IoT]] to internet
            - request for
                - self
                - other LAN device
                    - IGD too!
                - other WAN device (external)
        - DNS, DHCP, ...
- phishing /w fake UPnP device [^2]

## Source
- [[Sources|UPnP Forum: UPnP Device Architecture 2.0]]
- [[Sources|Blackhat: Universal Plug and Play IGD]]

[^1]: [[Sources|Blackhat: Universal Plug and Play IGD]]
[^2]: [[Code & Projects|evil-ssdp]]