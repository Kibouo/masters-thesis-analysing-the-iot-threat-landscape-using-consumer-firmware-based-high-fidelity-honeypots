# Protocols
## Layer 3 (Network)
- Zigbee
- Z-Wave

## Layer 7 (Application)
- [[UPnP]]
    - [[SSDP]]
- [[MQTT]]
- [[CoAP]]
- [[AMQP]]
- [[XMPP]]

### Comparison [^1] [^2] [^3]
<!-- TODO: more comparison, e.g. packet size/bandwidth consumption -->
<!-- See [^2], 10.1109/NICS48868.2019.9023812, 10.1109/ISSNIP.2014.6827678, ISSNIP.2014.6827678 -->
|                     | [[UPnP]]                | [[MQTT]]              | [[CoAP]]      | [[AMQP]]      | [[XMPP]]               |
| ------------------- | ----------------------- | --------------------- | ------------- | ------------- | ---------------------- |
| Network arch        | M2M                     | broker                | M2M [^!]      | M2M [^!]      | distr (C2S & S2S) [^#] |
| Communication model | req-resp & pub-sub [^@] | pub-sub               | req-resp [^!] | req-resp [^!] | req-resp [^#]          |
| QoS levels          | N/A                     | 3                     | 2             | 3             | N/A                    |
| Transport layer     | TCP & UDP [^@] [^%]/IP  | TCP/IP [^&]           | UDP/6LoWPAN   | TCP/IP [^&]   | TCP/IP                 |
| Data encoding       | XML                     | binary                | binary        | binary        | XML                    |
| Discovery           | yes                     | no                    | yes           | no            | no [^#]                |
| Encryption          | TLS                     | TLS [^*]              | DTLS, IPSec   | TLS, IPSec    | TLS [^*]               |
| Authentication      | no                      | basic & enhanced [^?] | SSL cert      | SASL          | SASL                   |
| Authorization       | no                      | ? [^?]                | SSL cert      | SASL          | SASL                   |

[^#]: extendable
[^!]: used (non-normative) to impl broker /w pub-sub
[^@]: eventing
[^%]: discovery
[^&]: any ordered, lossless, bi-directional connection
[^?]: specifics up to impl
[^*]: non-normative

## Source
[^1]: [[Sources|TrendMicro: The Fragility of Industrial IoT's Data Backbone]]
[^2]: [[Sources|Choice of Effective Messaging Protocols for IoT Systems: MQTT, CoAP, AMQP and HTTP]]
[^3]: [[Sources|A Survey of Application Layer Protocols for Internet of Things]]