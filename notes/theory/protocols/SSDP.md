# SSDP
- *Simple Service Discovery Protocol*
- HTTP client discovers HTTP service
- HTTP-like protocol
- networking
    - UDP
    - port: `1900`
    - addressing
        - multicast IPv4: `239.255.255.250`
        - unicast possible
            - not only response!

## Vulnerabilities
- [[reflection attack]] [^1]

## Source
- [[Sources|UPnP Forum: UPnP Device Architecture 2.0]]

[^1]: [[Sources|Cloudflare: Stupidly Simple DDoS Protocol (SSDP) generates 100 Gbps DDoS]]