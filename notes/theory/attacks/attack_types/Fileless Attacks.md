# Fileless Attacks
- ==
    - non-malware attack
    - zero-footprint attack

## What? [^1]
- attack using available utilities
    - runs in memory
        - no traces on disk
    - maybe filesystem as "support"
        - e.g.
            - registry key
            - file operation executes attack
                - file content unimportant

### Stages [^2]
- taxonomy in §3.3[^2] is more fitting as usage examples

#### 1. Intrusion
- same as [[malware-based Attacks]]

#### 2. Infection
- no payload fetched
    - no fingerprinting
- instead
    - reconnaissance
        - retrieve system info
            - e.g. `lscpu`, `netstat`, `ps`, `who`, `lastlog`, ...
    - take over system
        - alter password
        - de-immunization
            - remove system config
                - e.g. remove watchdog
            - prevent monitoring/auditing
                - e.g. kill processes (firewall)
        - convert to proxy
            - /w SSH tunnelling
                - prevent others by binding SSH port to `loopback`
                - **stealthy: no shell commands**

#### 3. Monetization
- same as [[malware-based Attacks]]
- also
    - steal data
        - e.g. `/etc/shadow`
    - abuse
        - launch network attack

### Taxonomy [^1]
![](../../../assets/2021-09-30-15-14-38.png)

#### Classification attack
1. entry-point
    - exploit
    - execution script/injection application
    - hardware
2. entry-point type
    - e.g.
        - file
        - network data
        - PCI
3. host
    - e.g.
        - Flash
        - document /w macro
        - shell script
        - hardware's firmware
        - infected USB

#### Types
- based on disk interaction
    - i.e. fingerprint-ability

1. no disk usage
    - main memory only
    - detection
        - hard
            - AV: no access to firmwares
    - IRL
        - uncommon
        - not easy/reliable
    - e.g.
        - network data
        - hardware's firmware
2. indirect file usage
    - malware runs in main memory
    - use hard-to-delete parts of disk
        - for "support"
            - e.g. add instruction to service
        - e.g. WMI repository
3. file usage for persistence
    - malware runs in main memory
    - use hard-to-delete parts of disk
        - for persistency
            - e.g. trigger, staged instructions, ...
        - e.g. registry

## Defence [^2]
- hard to detect
    - no disk traces
        - no forensic analysis
        - no AV file scanning
- firewall & AV
    - advanced techniques
        - **too heavy for IoT!**
    - techniques [^1]
        - script inspection
        - behaviour detection
        - memory scanning
            - DLL injection
        - boot sector protection
- improvements
    - prevent filesystem edit
        - read-only filesystem
        - default non-root user
    - CLI apps used a lot
        - shell command history auditing
            - problem: read-only filesystem
        - don't include unused tools
    - prevent intrusion
        - proper (strong & unique) passwords
        - don't expose services to internet
            - LAN + VPN only

## Source
- [[Sources|Microsoft: Out of sight but not invisible: Defeating fileless malware with behavior monitoring, AMSI, and next-gen AV]]

[^1]: [[Sources|Microsoft: Fileless threats]]
[^2]: [[Sources|Understanding Fileless Attacks on Linux-based IoT Devices with HoneyCloud]]
