# reflection attack
- abuse
    - data amplification
    - IP spoofing
- what?
    - increase traffic
- how?
    1. request data
        - spoof source IP
    2. response is bigger
        - sent to victim
