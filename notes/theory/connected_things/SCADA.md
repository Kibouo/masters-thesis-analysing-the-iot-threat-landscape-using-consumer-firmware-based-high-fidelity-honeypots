# SCADA
- *Supervisory Control and Data Acquisition*
- type of [[ICS]]
- centralized
    - single point of failure
- remote control
    - via web
    - multiple processes simultaneously
- working
    - collect data
        - /w sensors
    - display data
        - real-time
        - high-level UI (HMI)
    - allow control
        - real-time

## Source
- [[Sources|ENISA: ICS SCADA]]
- [[Sources|OSCE: Good Practices Guide on Non-Nuclear Critical Energy Infrastructure Protection (NNCEIP) from Terrorist Attacks Focusing on Threats Emanating from Cyberspace]]