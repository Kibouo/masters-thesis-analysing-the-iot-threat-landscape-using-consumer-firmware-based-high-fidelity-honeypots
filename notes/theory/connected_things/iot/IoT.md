# IoT
- *Internet of Things*
- networked "smart"/embedded devices
    - smart == contains computer
    - embedded == integrated in "normal" item
    - sensors
        - capture data
    - actuators
        - act upon physical world
- continuous cycle
    1. monitor
        - capture data
            - internet
            - sensors
                - connected
                - integrated
    2. control
        - automatic
        - config based
    3. act
- commercial/end-user focus [^1]
    - vs. [[ICS]]

## [[Home automation]]
<!-- TODO: architecture (cloud, device, app triangle) -->

## Vulnerabilities
- [[Attacks]]
- encryption
    - lack entropy
    - low computing resources
        - heavy cipher == hard
<!-- TOOD: update with [^2][^3] -->

## Source
- [[Sources|ENISA: Internet of Things (IoT)]]
<!-- TODO: update definitions: [[Sources|A Survey of Honeypots and Honeynets for Internet of Things, Industrial Internet of Things, and Cyber-Physical Systems]] -->

[^1]: personal addition
<!-- [^2]: [[Sources|IoT: Internet of Threats? A Survey of Practical Security Vulnerabilities in Real IoT Devices]] -->
<!-- [^3]: [[Sources|Baseline Security Recommendations for IoT]] -->