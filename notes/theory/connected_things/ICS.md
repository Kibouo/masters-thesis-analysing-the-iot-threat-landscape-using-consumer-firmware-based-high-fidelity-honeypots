# ICS
- *Industrial Control Systems*
- C&C network & systems
    - resources
        - 1st: PLCs
            - Programmable Logic Controllers
        - now: embedded devices
            - off-the-shelf
                - hardware
                - software
                    - OSS
            - == full computer
    - remote control --> [[SCADA]]
- continuous cycle
    1. monitor
        - capture data
    2. control
        - manual
        - automatic
    3. act
- critical infra focus
    - e.g. power plant, factory, ...
    - vs. [[IoT]]

## Source
- [[Sources|ENISA: ICS SCADA]]
- [[Sources|ENISA: Can we learn from SCADA security incidents?]]