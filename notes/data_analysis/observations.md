# Observations
Data range: 27/01/2022 - 12/04/2022

## Telnet proxy
- user and/or pass prepended with unicode characters
    - FALSE POSITIVE
        - shell gets messed up somehow
        - all input is also displayed double...

## Mirai-variants
- similar detection, setup, and execution
    1. find writeable directory
        - listing mounts, write to each entry
    -  optional: check environment is "real"
    2. find executable file to get payload with
        - base64, openssl base64
        - wget, curl, ftp
        - echo hex
    -  optional: check architecture
    3. dump payload & execute
- every action accompanied by "token"
    - name of payload or session number
- see
    - [[Sources|Mirai: A secure, open-source, modular, and extensible chat bot]]

## UPnP
- not much traffic
- only handful IPs
- wrong usage
    - GET `/Public_UPNP_gatedesc.xml`
    - POST `/Public_UPNP_gatedesc.xml` --> wrong path
    - leave
- VPS
    - what
        - GET base path
        - POST `/Public_UPNP_C3` attempting to bind random port to a Google IP
            - correct usage --> pre-check?
            - return 500
    - files: `2022-03-08_06:08:57.+0000.pcap.gz`, `2022-03-09_15:30:05.+0000.pcap.gz`

## Web server
- `for pcap in $(ls *.pcap.gz); do echo ${pcap}; tshark -r ${pcap} -Y 'http && ip.src_host != 109.132.31.175 && frame contains YWRtaW46YWRtaW4'; done` shows some accesses
- manual as they all slightly differ
- just browsing, not for long. No config edits
- changing user agents

## Weird data
- `/mnt/2A86CD0086CCCE0F/mihaly_csonka_honeypot_logs/ec2-54-78-156-156.eu-west-1.compute.amazonaws.com/tcpdump/2022-02-14_15`: tcp.stream == 2874
