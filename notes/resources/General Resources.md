# General Resources
- [Timesheet](https://docs.google.com/spreadsheets/d/11qHKWB2qdS7rDp9kpE2X7XakBzfS0pj0RnwhnjLMEZA)
- [GitHub](https://github.com/INF-Vakken-Masterproef-21-22/inf-masterproef-21-22-student-MihalyC-1644219)
    - meeting reports
    - code
- [Overleaf](https://www.overleaf.com/project/614897c3f127d41093d6f347)
- [Instructions](https://docs.google.com/document/d/1aPEG9DQf9JERIylYJe5LSR3r_UnuOM3pRB38vbga5Pk/edit)

## Other
- [Proposal/description](https://docs.google.com/document/d/1m7XtOUCzHgHE9CLnADKaUTPTbYUnEVbxNWP7ipQO2K0/edit)
- [Final assignment subjects](https://docs.google.com/spreadsheets/d/18YizeKAaq_00Y-vCFP6wyjrnsRYOErKi2VFejZB6yy4/)
- [Halfway status update presentation](https://docs.google.com/presentation/d/1JuwNwip7NuHYNBunl6r2pBXlMoFyZNaoOkWW4r-ScS0/)
