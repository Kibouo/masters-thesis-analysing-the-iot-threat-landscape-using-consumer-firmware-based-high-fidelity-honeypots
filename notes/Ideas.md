# Ideas
## Goal
- setup and/or create honeypot
- business env?
- honeypot --> CVEs on purpose
    - finding 0days would be nice 😁
- 'special'?
    - IoT == small devices & highly connected
        - multiple diff devices
        - multiple diff protocols
    - distribute creds to different audiences
- source attacks
    - internet only
    - not physical/close proximity

## Analysis
- who, what, where, when, how?
    - not only malware, also scans
- what *not* (missing?)
- classify attacks
    - type (botnet, ransom, ...)
    - what is being attacked?
- source audience differences
    - e.g. audience X --> prevalently SSH --> prevalently Y malware --> only x86?
- multiple types of files they can exfiltrate
    - what is interesting
    - based on audience again?

### Explicit questions
- relation probing & attack?
- targeted attack or spray & pray?

#### How
- how was payload delivered?
    - directly
    - C&C/staged/3rd party
- how does attack work?
- quantify attacks

#### What
- what is the payloads/attack?
    - modified version of something?
        - homology diagram
- entry or take-over?
- what is attacked?
    - HW vuln?
    - firmware?
    - architecture?
    - device specific?
    - IoT or generic exploit?

#### Who
- which audience attacked?
- fingerprint
    - IP
    - user agent
    - ...

#### Where
- what entry points?
- pivot path?

#### When
- chronological steps
- more/less attacks during some time frame?

## Difficulties
- get attacks
    - due to device
        - IoT devices == heterogeneous
        - scanning specific version
            - offload to shodan?
    - due to IoT network
        - automated scripts might not pivot
- diff creds/audience
    - spreading them
        - E.g. pastebin/hastebin, github, shodan, discord, reddit, etc.
    - tracking them
    - differentiate from random
- data logging
- not suspicious

## Contributions
- attack landscape analysis
    - devices
    - IoT protocols
- audience analysis
    - any correlations? --> **homology diagram**
