# Info
- Question: what is the state of the IoT threat-landscape in 2021-2022
    - what is being exploited?
    - what malware is being spread?
    - how "deep" do adversaries pivot into an IoT network?
    - when are adversaries most active?
    - does the background of the adversary impact any of the above?
- Type: qualitative research